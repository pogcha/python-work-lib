import numpy as np
import python_lib.poly_2d as p2
import matplotlib.pyplot as plt
import python_lib.smearBib as smear
# test 2d fit

# generate some data on a regular mesh with some noise
x = np.linspace(0, 1, 20)
y = np.linspace(0, 1.5, 25)
X, Y = np.meshgrid(x, y, copy=False)


# Choose either of the two Z functions (by commenting out one):

# First example: We Fit a polynomial with noise.
Z = 3.0*X**2 + 0.1*X + Y**2 + X*Y * 0.1*np.exp(X*Y) + np.random.rand(*X.shape)*0.01

# Second example: We choose something with a higher rank polynomial than
# the fit function to show the superirity of a local approach (Savitzky-Golay smoothing)
# against fitting a function globally
Z =   8.0*(X-0.5)**3  + 8.0*(Y-1.2)**3+ np.random.rand(*X.shape)*0.01


# make some dummy error estimate
Zerr = np.ones(Z.shape)

# take the derivative numerically
Zgrad_list = np.gradient(Z)
Zgrad_y = Zgrad_list[0]/np.gradient(y)[:,np.newaxis]
Zgrad_x = Zgrad_list[1]/np.gradient(x)[np.newaxis,:]

# do the 2d fit with a polynomial with rank 2 for x and rank 2 for y
coeffs = p2.polyfit_2d(x,y,Z,Zerr,2,2)

# evaluate the fitted coefficents
Zfit = p2.evaluate_poly_2d(x,y,coeffs)
print('fitted coeffs a_nm in a_00     + a_01 y     + a_02 y^2')
print('                      a_20 x   + a_11 y x   + a_12 y^2 x')
print('                      a_20 x^2 + a_21 y x^2 + a_22 y^2 x^2' )
print('')
print(coeffs[:,:])
gradCoeffs_x,gradCoeffs_y = p2.grad_poly_2d(coeffs)

print('')
print('derivative w.r.t x from fit as coeffs:')
print('')
print(gradCoeffs_x)

print('')
print('derivative w.r.t y from fit as coeffs:')
print('')
print(gradCoeffs_y)
Gradfit_x = p2.evaluate_poly_2d(x,y,gradCoeffs_x)
Gradfit_y = p2.evaluate_poly_2d(x,y,gradCoeffs_y)

yGal,yGalDeriv_x,yGalDeriv_y = smear.galitskyNonUnifX_2D(x,y,Z,Zerr,0.3,0.3,2,2)


# some plots
plt.figure(1)
plt.subplot(1,3,1)
minValGlobal = np.min(Zgrad_y)
maxValGlobal = np.max(Zgrad_y)
plt.contourf(X,Y,Zgrad_y,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('grad_y by fin. diff.')
plt.subplot(1,3,2)
plt.contourf(X,Y,Gradfit_y,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('grad_y by ana. deriv.')
plt.subplot(1,3,3)
plt.contourf(X,Y,yGalDeriv_y,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('grad_y by sav-gol')
#plt.colorbar()
plt.tight_layout()

plt.figure(2)
plt.subplot(1,3,1)
minValGlobal = np.min(Zgrad_x)
maxValGlobal = np.max(Zgrad_x)
plt.contourf(X,Y,Zgrad_x,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('grad_x by fin. diff.')
plt.subplot(1,3,2)
plt.contourf(X,Y,Gradfit_x,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('grad_x by ana. der.')
plt.subplot(1,3,3)
plt.contourf(X,Y,yGalDeriv_x,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('grad_x by sav-gol')
#plt.colorbar()
plt.tight_layout()


plt.figure(3)
plt.subplot(1,3,1)
minValGlobal = np.min(Z)
maxValGlobal = np.max(Z)
plt.contourf(X,Y,Z,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('raw data')
plt.subplot(1,3,2)
plt.contourf(X,Y,Zfit,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('fit function')
plt.subplot(1,3,3)
plt.contourf(X,Y,yGal,vmin=minValGlobal,vmax=maxValGlobal)
plt.title('sav. Golay')
#plt.colorbar()
plt.tight_layout()
plt.show()