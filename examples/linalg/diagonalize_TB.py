"""
In this example the back and forth transformation of matrices is done

Matrices are diagonalized. The Eigenvectors are then used to transform
from eigen basis to the original basis ans vice versa

First, an example is used where broadcasting applies (MoS2 band strucure)

Second, a simple matrix without broadcasting
"""


import numpy as np
import python_lib.linalg as myLinAlg



# definition of k vectors in the brillouin zone
nKBZ = 50
M = np.array([np.pi,np.pi/np.sqrt(3)])
K = np.array([4*np.pi/3.0,0.0])
b1 = 2.0*np.array([M[0],-M[1]])
b2 = 2.0*np.array([K[1],K[0]])
kBZ = np.zeros((nKBZ**2,2))
count = 0
for ik in range(nKBZ):
    for jk in range(nKBZ):
        
        kBZ[count,:] = b1*ik/float(nKBZ) + b2*jk/float(nKBZ)
        #print(ik,jk,kBZ[count,:],b1*ik/float(nKBZ),b2*jk/float(nKBZ))
        #plt.plot(kBZ[count,0],kBZ[count,1],'x')
        count += 1
        
def Hk(kVec):
    # definition of the tight binding hamiltonian
    alpha = 0.5*kVec[:,0]
    beta = np.sqrt(3)*0.5*kVec[:,1]
    
    eps1 = 0.683
    eps2 = 1.707
    t0 = -0.146
    t1 = -0.114
    t2 = 0.506
    t11 = 0.085
    t12 = 0.162
    t22 = 0.073
    r0 = 0.060
    r1 = -0.236
    r2 = 0.067
    r11 = 0.016
    r12 = 0.087
    u0 = -0.038
    u1 = 0.046
    u2 = 0.001
    u11 = 0.266
    u12 = -0.176
    u22 = -0.150
    

    
    
    
    V0 = (
        eps1 + 2.0*t0 * (2.0*np.cos(alpha)*np.cos(beta) + np.cos(2.0*alpha))
        +2.0*r0*(2.0*np.cos(3.0*alpha)*np.cos(beta)+np.cos(2.0*beta))
        +2.0*u0*(2.0*np.cos(2.0*alpha)*np.cos(2.0*beta)+np.cos(4.0*alpha))
        )
        
    V1 = ((
        -2.0*np.sqrt(3.0)*t2*np.sin(alpha)*np.sin(beta)
        +2.0*(r1+r2)*np.sin(3.0*alpha)*np.sin(beta)
        -2.0*np.sqrt(3.0)*u2*np.sin(2.0*alpha)*np.sin(2.0*beta)
        )+
        1j*(
            2.0*t1*np.sin(alpha)*(2.0*np.cos(alpha)+np.cos(beta))
            +2.0*(r1-r2)*np.sin(3.0*alpha)*np.cos(beta)
            +2.0*u1*np.sin(2.0*alpha)*(2.0*np.cos(2.0*alpha)+np.cos(2.0*beta))
        ))
    V2 = ((
        2.0*t2*(np.cos(2.0*alpha) - np.cos(alpha)*np.cos(beta))
        -2.0/np.sqrt(3.0)*(r1+r2)*(np.cos(3.0*alpha)*np.cos(beta) - np.cos(2.0*beta))
        +2.0*u2*(np.cos(4*alpha)-np.cos(2.0*alpha)*np.cos(2.0*beta))
        )+
        1j*(2.0*np.sqrt(3.0)*t1*np.cos(alpha)*np.sin(beta)
        +2.0/np.sqrt(3.0)*np.sin(beta)*(r1-r2)*(np.cos(3.0*alpha)+2.0*np.cos(beta))
        +2.0*np.sqrt(3.0)*u1*np.cos(2.0*alpha)*np.sin(2.0*beta)
    ))
    
    V11 = (eps2 + (t11+3.0*t22)*np.cos(alpha)*np.cos(beta) + 2.0*t11*np.cos(2.0*alpha)
        +4.0*r11*np.cos(3.0*alpha)*np.cos(beta) + 2.0*(r11+np.sqrt(3)*r12)*np.cos(2.0*beta)
        +(u11+3.0*u22)*np.cos(2.0*alpha)*np.cos(2.0*beta) + 2.0*u11*np.cos(4.0*alpha)
        )
    V12 = ((
            np.sqrt(3.0)*(t22-t11)*np.sin(alpha)*np.sin(beta) 
            + 4.0*r12*np.sin(3*alpha)*np.sin(beta)
            +np.sqrt(3.0)*(u22-u11)*np.sin(2.0*alpha)*np.sin(2.0*beta)
            )+
            1j*(
             4.0*t12*np.sin(alpha)*(np.cos(alpha) - np.cos(beta))
            +4.0*u12*np.sin(2.0*alpha)*(np.cos(2.0*alpha) - np.cos(2.0*beta))
            ))
    V22 = (eps2 + (3.0*t11+t22)*np.cos(alpha)*np.cos(beta)
            +2.0*t22*np.cos(2.0*alpha)
            +2.0*r11*(2.0*np.cos(3.0*alpha)*np.cos(beta)+np.cos(2*beta))
            +2.0/np.sqrt(3.0)*r12*(4.0*np.cos(3.0*alpha)*np.cos(beta)-np.cos(2*beta))
            +(3.0*u11+u22)*np.cos(2*alpha)*np.cos(2.0*beta)
            +2.0*u22*np.cos(4.0*alpha)
    )
    
    ham = np.zeros((kVec.shape[0],3,3),dtype=complex)
    ham[:,0,0] = V0
    ham[:,1,0] = V1
    ham[:,0,1] = V1.conjugate()
    ham[:,2,0] = V2
    ham[:,0,2] = V2.conjugate()
    ham[:,1,1] = V11
    ham[:,2,1] = V12
    ham[:,1,2] = V12.conjugate()
    ham[:,2,2] = V22    
    return ham


# set up hamiltonian
hamBZ = Hk(kBZ)
# diagonalization on all k points
eigValsBZ,eigVecsBZ = np.linalg.eigh(hamBZ) 

# blow up eigenvalues to diagonal matrices
eigValsEye = np.zeros((eigValsBZ.shape[0],3,3))
for ik in range(eigValsBZ.shape[0]):
    eigValsEye[ik,:,:] = np.diag(eigValsBZ[ik,:])
    
    
# do some testing (please check for your self):
print('\nTest1: from eigen basis to orig basis (broadcasted)')
matty = myLinAlg.fromDiagToOrig(eigVecsBZ,eigValsEye)
print('result:')
print(matty[1,:,:])
print('expected:')
print(hamBZ[1,:,:])

print('\nTest2: from eigen basis to orig basis, starting from diagonal matrix (broadcasted)')
    
matty = myLinAlg.fromDiagToOrig_diagonalStart(eigVecsBZ,eigValsBZ)
print('result:')
print(matty[1,:,:])
print('expected:')
print(hamBZ[1,:,:])

print('\nTest3: from orig basis to eigen basis (broadcasted)')
matty2 = myLinAlg.fromOrigToDiag(eigVecsBZ[1,:,:],hamBZ[1,:,:])
print(matty2[:,:])
print(np.diag(eigValsBZ[1,:]))

matrix = np.array([[1, 2, 3-1j],[2, 1, 4],[3+1j, 4, 4]],dtype=complex)

eigValsSimple,eigVecsSimple = np.linalg.eigh(matrix) 

print('\nTest4: from eigen basis to orig basis (single matrix)')
mattySimple = myLinAlg.fromDiagToOrig(eigVecsSimple,np.diag(eigValsSimple))
print(mattySimple)
print(matrix)
print('\nTest5: from eigen basis to orig basis, starting from diagonal matrix (single matrix)')
mattySimple = myLinAlg.fromDiagToOrig_diagonalStart(eigVecsSimple,eigValsSimple)
print(mattySimple)
print(matrix)
print('\nTest6: from orig basis to eigen basis (single matrix)')
mattySimple2 = myLinAlg.fromOrigToDiag(eigVecsSimple,matrix)
print(mattySimple2)
print(np.diag(eigValsSimple))


