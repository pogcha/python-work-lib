import numpy as np
import python_lib.coulomb as coulomb

F0 = 5.0
F2 = 2.1
F4 = 0.777
F6 = 0.33

n_orbs = 5

# here first the coulomb matrix in spherical basis is calculated
# and then it is transformed to cubic basis
# if you are interested only in the cubic basis you can also directly
# calculate it by using basis='cubic' in the next line
u_mat_s = coulomb.set_u_matrix(n_orbs,[F0, F2, F4, F6],basis='spherical')
u_mat = coulomb.transform_umatrix(u_mat_s,
                                  coulomb.get_spherical2cubic_trans(n_orbs))


# here we output the Coulomb tensor in spherical basis in a format
# which can be used as an input for jindras ED code.
# I have tested that this gives the same result as using F0 F2 F4
# directly in the ED code
# i.e., Use spherical and order of second and third index is swapped

with open('Umatrix.dat','w') as f:
    f.write('0\n')
    for ii in range(n_orbs):
        for jj in range(n_orbs):
            for kk in range(n_orbs):
                for ll in range(n_orbs):
                    # here the second and third index are swapped!
                    # thats the way jindra's code wants it!
                    this = u_mat_s[ii,kk,jj,ll] # correct
                    # this = u_mat_s[ii,jj,kk,ll] # wrong
                    f.write( '0 0 {:d} {:d} {:d} {:d} {: 19.15f}\n'
                                .format(ii+1,jj+1,kk+1,ll+1, this.real))


# here we output the tensor in cubic and spherical basis in a more readable format
with open('cubic.dat','w') as f:
    for ii in range(n_orbs):
        for jj in range(n_orbs):
            for kk in range(n_orbs):
                for ll in range(n_orbs):
                    this = u_mat[ii,jj,kk,ll]
                    if np.abs(this) > 1e-12:
                        f.write( '{:d} {:d} {:d} {:d} {: 19.15f}\n'
                                    .format(ii+1,jj+1,kk+1,ll+1, this.real))
                        
with open('spher.dat','w') as f:
    for ii in range(n_orbs):
        for jj in range(n_orbs):
            for kk in range(n_orbs):
                for ll in range(n_orbs):
                    this = u_mat_s[ii,jj,kk,ll]
                    if np.abs(this) > 1e-12:
                        f.write( '{:d} {:d} {:d} {:d} {: 19.15f}\n'
                                    .format(ii+1,jj+1,kk+1,ll+1, this.real))
