# -*- coding: utf-8 -*-
"""
In this example the density of states is calculated for a H(k) 
for NbSe2, 3 band model.

First, the non-interacting DOS is calculated. From that and the total
electron number, the fermi energy is calculated

Second, the interacting DOS is calculated assuming an atomic self-energy

After all, the VASP DOS, non-interacting DOS and interacting DOS are plotted.

Third, the hybridization function of the first orbital is calculated
"""
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import python_lib.greens_functions as green
import python_lib.hk as hk
import python_lib.vasp_parse as vasp_parse

# total number of electrons
nElecs = 5.00

# parameters to deinfe energy axis for non interacting DOS:
numW0 = 4000
minE0 = -0.42
maxE0 = 0.35

# parameters to deinfe energy axis for non interacting DOS:
numW = 1000
minE = -0.42
maxE = 0.35

# broadening: 
idelta = 0.0015

num_cpus = 4
    
def Fermienergy(w,dos,tot,numOrbs):
    print('norm should be',numOrbs*2.0)
    print('norm is',np.sum(dos)*(w[1]-w[0])*2.0)
    print('renormalizing')
    dos /= np.sum(dos)*(w[1]-w[0])/numOrbs/2.
    dosA = np.sum(dos,axis=1)
    cumm = np.cumsum(dosA)*(w[1]-w[0])
    #print(np.abs(cumm-nElecs))
    #plt.plot(w,cumm-nElecs,'x')
    
    index = np.argmin(np.abs(cumm-tot))
    if cumm[index]< tot:
        x1 = w[index]
        x2 = w[index+1]
        y1 = cumm[index]-tot
        y2 = cumm[index+1]-tot
    else:
        x1 = w[index-1]
        x2 = w[index]
        y1 = cumm[index-1]-tot
        y2 = cumm[index]-tot
    m = (y2-y1)/(x2-x1)
    c = y2-m*x2
    eF = -c/m
    return dos,eF


Hk, nD, nP, nAt, kVec, nE = hk.readHk('Hk_18x18.txt',1)

w0 = np.linspace(minE0, maxE0 ,numW0)
numOrbsg = Hk.shape[1]


mu_DFT = -2.36585
wVASP,dosVASP = vasp_parse.readDOS('vaspStuff/dost.dat')
wVASP,pDOSVASP = vasp_parse.readpDOS('vaspStuff/',range(1,39),False)


dosGreen0,eigVecs,eigVals = green.non_interacting_greens_vecK(w0+1j*idelta,mu_DFT,Hk,numOrbsg,calcDOS=True,cpu_count=num_cpus,kSum=True)
print(dosGreen0.shape)
dosGreen0,eF0 = Fermienergy(w0,dosGreen0,nElecs,numOrbsg)
print('fermienergy 0',eF0)

# these are parameters from a fit to a DMFT calculation
# and gives an atomic self-energy. 
# beta = 300
U=0.4
c= 0.20199534
w00=-0.51484874
mu_dc=-0.01
mu=-2.1841474557607374

w = np.linspace(minE, maxE ,numW)

# we generate a matrix which has the same shape as Hk: the self energy
# this matrix has only entries in the d-block
sigma = c + U**2/(4.0*(w+1j*idelta) + w00)
sigmaMat = np.zeros((w.size,Hk.shape[1],Hk.shape[1]),dtype=complex)
sigmaMat[:,0,0] = sigma + mu_dc

dosGreen2 = green.interacting_greens_vecK(w+1j*idelta, mu, Hk, sigmaMat,calcDOS=True)

# now we calculate the hybridization function:
# we recycle the eigenvectors and eigenvalues which were calculated for 
# the density of states.
# this is of course a bit non-sense. We could  have just calculated the
# full greensfunction and extracted the DOS and the hybridization function
# from it. This is more like a feature demo :-)
GreenFull,_,_ = green.non_interacting_greens_vecK(w0+1j*idelta,mu_DFT,Hk,Hk.shape[1],calcDOS=False,diagonal=False,eigVecs=eigVecs,eigVals=eigVals,cpu_count=num_cpus)
Delta = green.hybridization(w0, GreenFull,range(GreenFull.shape[1]))


if True:
    plt.figure(1,figsize=(4.0,4.5))
    topBottom = 0.09
    margin=0.01
    left = 0.1
    right = 0.9
    
    ax = plt.subplot(3,1,1)
    plt.text(0.03,0.88,'a) GGA',transform=ax.transAxes,horizontalalignment='left')


    plt.plot(wVASP,np.sum(pDOSVASP[:,:,:    ],axis=(1,2)),'k',label='total')
    plt.plot(wVASP,np.sum(pDOSVASP[:,:,[4,5,6,7,8]],axis=(1,2)),color='C0',label='$d$')
    plt.plot(wVASP,np.sum(pDOSVASP[:,:,6],axis=(1)),'--C0',label='$d_{z^2}$')
    plt.plot(wVASP,np.sum(pDOSVASP[:,:,[1,2,3]],axis=(1,2)),'C2',label='$p$')
    
    ax.set_xticklabels([])
    plt.yticks([])
    plt.ylabel('DOS')
    plt.legend(frameon=False)
    plt.xlim(minE0,maxE0)
    ax = plt.subplot(3,1,2)
    plt.text(0.03,0.88,'b) GGA transformed',transform=ax.transAxes,horizontalalignment='left')
    plt.plot(w0,np.sum(dosGreen0[:,:],1),'k',label='total')
    plt.plot(w0,np.sum(dosGreen0[:,:1],1),'C0',label='correlated')
    plt.plot(w0,np.sum(dosGreen0[:,1:],1),'C1',label='uncorrelated')
    plt.xlim(minE0,maxE0)
    plt.ylabel('DOS')
    plt.yticks([])
    ax.set_xticklabels([])
    plt.legend(frameon=False,loc='upper right')

    ax=plt.subplot(3,1,3)
    plt.text(0.03,0.88,'c) DMFT',transform=ax.transAxes,horizontalalignment='left')
    plt.plot(w,np.sum(dosGreen2[:,:],1),'k',label='total')
    plt.plot(w,np.sum(dosGreen2[:,:1],1),'C0',label='correlated')
    plt.plot(w,np.sum(dosGreen2[:,1:],1),'C1',label='uncorrelated')

    plt.xlabel('w')
    plt.ylabel('DOS')
    plt.yticks([])
    plt.xlim(minE,maxE)
    
    plt.legend(frameon=False,loc='lower left',bbox_to_anchor=(0.4,0.3))
    plt.tight_layout()
    plt.figure(2)
    plt.plot(w0,Delta[:,0,0].imag)
    plt.xlabel('w')
    plt.ylabel('imag$ \Delta(w)$')
    plt.show()

    plt.show()

