
import numpy as np
import python_lib.poly_2d as p2

def smearMulti(energy,data,sig):
    dE = np.gradient(energy)
    expFunc = ( 1./(sig*np.sqrt(2*np.pi))
                *np.exp(-(energy[:,np.newaxis]-energy[np.newaxis,:])**2
                         /(2*sig**2))
             )
    specSmear = np.sum(data[np.newaxis,:,:]*expFunc[:,:,np.newaxis]
                            *dE[np.newaxis,:,np.newaxis],axis=1)
    return specSmear

def smear(energy,data,sig):
    dE = np.gradient(energy)
    expFunc = ( 1./(sig*np.sqrt(2*np.pi))
                *np.exp(-(energy[:,np.newaxis]-energy[np.newaxis,:])**2
                /(2*sig**2))
            )
    specSmear = np.sum(data[np.newaxis,:]*expFunc[:,:]
                        *dE[np.newaxis,:],axis=1)

    return specSmear
    
    
# old version.. superseeded by the one below which includes kernel and
# possibility to calucate smoothed version on a finer grid
# new version is backwards compatible
def galitskyNonUnifX_old(x,y,yerr,box,poly):
    
    # perform galitsky-solvay smoothing of y = f(x)
    # Anal. Chem., 1964, 36 (8), pp 1627-1639
    # DOI: 10.1021/ac60214a047
    # Publication Date: July 1964

    # here x can be non-uniform. Box is the region in which 
    # polynomials of rank poly are fitted

    # y is the data
    # yerr is the error of the data

    yGal = np.zeros(y.size)
    yGalDeriv = np.zeros(y.size)
    for ix in range(y.size):
        xInd = np.where(np.abs(x[ix]-x)<box)
        # box is defined. now do a polyfit
        pFit = np.polyfit(x[xInd],y[xInd],poly,w=1.0/yerr[xInd])
        yGal[ix] = np.polyval(pFit,x[ix])
        yGalDeriv[ix] =  np.polyval(np.polyder(pFit),x[ix])
    return yGal,yGalDeriv
    

def galitskyNonUnifX(x,y,yerr,box,poly,kernel=None,xfine=None):
    
    # perform galitsky-solvay smoothing of y = f(x)
    # Anal. Chem., 1964, 36 (8), pp 1627–1639
    # DOI: 10.1021/ac60214a047
    # Publication Date: July 1964

    # here x can be non-uniform. Box is the region in which 
    # polynomials of rank poly are fitted

    # y is the data
    # yerr is the error of the data

    # the data can be weighted with a function which puts
    # more weight on data close the the estimated data.
    # see https://en.wikipedia.org/wiki/Kernel_(statistics)#Kernel_functions_in_common_use
    # for further examples. Here a tricubic function and no weighting is implemented

    # the smoothened data can also be calculated on a different grid than the
    # original one. Use xfine...

    def cub(x,x0,sigma):
        return (1.0 - np.abs( ( x - x0 ) / sigma )**3 )**3    
    def none(x,x0,sigma):
        return np.ones(x.size)
        
    if xfine is None:
        xfine = 1.0*x
    else:
        assert np.max(xfine) <= np.max(x)
        assert np.min(xfine) >= np.min(x)

    
    if kernel is None:
        kernel_func = none
    elif kernel == 'cub':
        kernel_func = cub
    else:
        raise Exception('only kernel=cub or None implemented')

    yGal = np.zeros(xfine.size)
    yGalDeriv = np.zeros(xfine.size)
    for ix in range(xfine.size):
        xInd = np.where(np.abs(xfine[ix]-x)<box)
        # box is defined. now do a polyfit
        pFit = np.polyfit(x[xInd],y[xInd],poly,w=1.0/yerr[xInd]*kernel_func(x[xInd],xfine[ix],box))
        yGal[ix] = np.polyval(pFit,xfine[ix])
        yGalDeriv[ix] =  np.polyval(np.polyder(pFit),xfine[ix])
    return yGal,yGalDeriv
 


# superseded by new version below
# the new version is backward compatible
def galitskyNonUnifX_2D_old(x,y,z,zerr,box_x,box_y,poly_x,poly_y):
    
    # perform galitsky-solvay smoothing of y = f(x)
    # Anal. Chem., 1964, 36 (8), pp 1627–1639
    # DOI: 10.1021/ac60214a047
    # Publication Date: July 1964

    # here x and y can be non-uniform. Box is the region in which 
    # polynomials of rank poly are fitted

    # z is the data
    # zerr is the error of the data

    # test of dimensions:
    assert x.size == z.shape[0]
    assert y.size == z.shape[1]
    assert z.shape == zerr.shape

    yGal = np.zeros(z.shape)
    yGalDeriv_x = np.zeros(z.shape)
    yGalDeriv_y = np.zeros(z.shape)
    for ix in range(x.size):
        xInd = np.where(np.abs(x[ix]-x)<box_x)
        x_indees = np.arange(x.size)[xInd]
        xIndStart = x_indees[0]
        xIndEnd = x_indees[-1]+1
        for iy in range(y.size):            
            yInd = np.where(np.abs(y[iy]-y)<box_y)
            # boolean indexing gives a 1D array for 2D objects
            # we have to do a fix here:
            y_indees = np.arange(y.size)[yInd]
            yIndStart = y_indees[0]
            yIndEnd = y_indees[-1]+1
            # box is defined. now do a polyfit
            pFit = p2.polyfit_2d(x[xInd],y[yInd],z[xIndStart:xIndEnd,yIndStart:yIndEnd],zerr[xIndStart:xIndEnd,yIndStart:yIndEnd],poly_x,poly_y)

            yGal[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFit)
            pFitGrad_x = p2.grad_poly_2d(pFit)[0]
            pFitGrad_y = p2.grad_poly_2d(pFit)[1]
            yGalDeriv_x[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFitGrad_x)
            yGalDeriv_y[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFitGrad_y)
    return yGal,yGalDeriv_x,yGalDeriv_y
    
def galitskyNonUnifX_2D(x,y,z,zerr,box_x,box_y,poly_x,poly_y,xfine=None,yfine=None,kernel=None):
    
    # perform galitsky-solvay smoothing of y = f(x)
    # Anal. Chem., 1964, 36 (8), pp 1627–1639
    # DOI: 10.1021/ac60214a047
    # Publication Date: July 1964

    # here x and y can be non-uniform. Box is the region in which 
    # polynomials of rank poly are fitted

    # xfine and yfine can be the smoothed function on a new (finer) grid

    # kernel can be a weight function putting more weight on data points
    # closer to the current x,y where the fit is evaluated

    # z is the data
    # zerr is the error of the data

    # test of dimensions:
    assert x.size == z.shape[0]
    assert y.size == z.shape[1]
    assert z.shape == zerr.shape
    
    def cub(x,y,x0,y0,sigmax,sigmay):
    
        # the distance is defined as the maximum of the linear distances
        # d = max( (x-x0)/sigmax, (y-y0)/sigmay )
    
        absx = np.abs((x-x0)/sigmax)
        absy = np.abs((y-y0)/sigmay)
        
        distance = np.max(np.array([absx[:,np.newaxis]*np.ones(y.size)[np.newaxis,:],
                                    absy[np.newaxis,:]*np.ones(x.size)[:,np.newaxis]])
                          ,axis=0)
                          
        return ( 1.0- distance**3 )**3
        
    def none(x,y,x0,y0,sigmax,sigmay):
        return 1.0
        
    if xfine is None:
        xfine = 1.0*x
    else:
        assert np.max(xfine) <= np.max(x)
        assert np.min(xfine) >= np.min(x)
        
    if yfine is None:
        yfine = 1.0*y
    else:
        assert np.max(yfine) <= np.max(y)
        assert np.min(yfine) >= np.min(y)

    if kernel is None:
        kernel_func = none
    elif kernel == 'cub':
        kernel_func = cub
    else:
        raise Exception('only kernel=cub or None implemented')

    yGal = np.zeros((xfine.size,yfine.size))
    yGalDeriv_x = np.zeros((xfine.size,yfine.size))
    yGalDeriv_y = np.zeros((xfine.size,yfine.size))
    for ix in range(xfine.size):
        xInd = np.where(np.abs(xfine[ix]-x)<box_x)
        x_indees = np.arange(x.size)[xInd]
        xIndStart = x_indees[0]
        xIndEnd = x_indees[-1]+1
        for iy in range(yfine.size):
            yInd = np.where(np.abs(yfine[iy]-y)<box_y)
            # boolean indexing gives a 1D array for 2D objects
            # we have to do a fix here:
            y_indees = np.arange(y.size)[yInd]
            yIndStart = y_indees[0]
            yIndEnd = y_indees[-1]+1
            # box is defined. now do a polyfit
            distance_weight = kernel_func(x[xInd],y[yInd],xfine[ix],yfine[iy],box_x,box_y)
            pFit = p2.polyfit_2d(x[xInd],y[yInd],z[xIndStart:xIndEnd,yIndStart:yIndEnd],zerr[xIndStart:xIndEnd,yIndStart:yIndEnd]/distance_weight,poly_x,poly_y)
            
            yGal[ix,iy] = p2.evaluate_poly_2d(xfine[ix],yfine[iy],pFit)
            pFitGrad_x = p2.grad_poly_2d(pFit)[0]
            pFitGrad_y = p2.grad_poly_2d(pFit)[1]
            yGalDeriv_x[ix,iy] = p2.evaluate_poly_2d(xfine[ix],yfine[iy],pFitGrad_x)
            yGalDeriv_y[ix,iy] = p2.evaluate_poly_2d(xfine[ix],yfine[iy],pFitGrad_y)
    return yGal,yGalDeriv_x,yGalDeriv_y    
def galitskyNonUnifX_2D_free_energy(x,y,zD,zDerr,zG,zGerr,box_x,box_y,poly_x,poly_y):
    
    # perform galitsky-solvay smoothing of y = f(x)
    # Anal. Chem., 1964, 36 (8), pp 1627–1639
    # DOI: 10.1021/ac60214a047
    # Publication Date: July 1964

    # here x and y can be non-uniform. Box is the region in which 
    # polynomials of rank poly are fitted

    # z is the data
    # zerr is the error of the data

    # test of dimensions:
    assert x.size == zD.shape[0]
    assert y.size == zD.shape[1]
    assert zD.shape == zDerr.shape
    assert zG.shape == zGerr.shape
    assert zG.shape == zD.shape

    yGalD = np.zeros(zD.shape)
    yGalDerivD_x = np.zeros(zD.shape)
    yGalDerivD_y = np.zeros(zD.shape)
    
    yGalG = np.zeros(zD.shape)
    yGalDerivG_x = np.zeros(zD.shape)
    yGalDerivG_y = np.zeros(zD.shape)
    
    for ix in range(x.size):
        print(ix,x.size)
        xInd = np.where(np.abs(x[ix]-x)<box_x)
        #print(xInd)
        x_indees = np.arange(x.size)[xInd]
        xIndStart = x_indees[0]
        xIndEnd = x_indees[-1]+1
        #print('xBox',x[xInd])
        #print('xBox alt',x[xIndStart:xIndEnd])
        for iy in range(y.size):            
            yInd = np.where(np.abs(y[iy]-y)<box_y)
            # boolean indexing gives a 1D array for 2D objects
            # we have to do a fix here:
            y_indees = np.arange(y.size)[yInd]
            yIndStart = y_indees[0]
            yIndEnd = y_indees[-1]+1
            # box is defined. now do a polyfit
            #p2.polyfit_2d(x,y,Z,Zerr,2,2,)
            #print('yBox',y[yInd],y[yInd].shape)
            #print('yBox alt',y[yIndStart:yIndEnd],y[yIndStart:yIndEnd].shape)
            #print('indis',yIndStart,yIndEnd)
            #print('shape z',z[xIndStart:xIndEnd,yIndStart:yIndEnd].shape)
            #print('shape zerr',zerr[xIndStart:xIndEnd,yIndStart:yIndEnd].shape)
            #print('shape x',x[xInd].shape)
            #print('shape y',y[yInd].shape)
            startD = p2.polyfit_2d(x[xInd],y[yInd],zD[xIndStart:xIndEnd,yIndStart:yIndEnd],zDerr[xIndStart:xIndEnd,yIndStart:yIndEnd],poly_x,poly_y)
            startG = p2.polyfit_2d(x[xInd],y[yInd],zD[xIndStart:xIndEnd,yIndStart:yIndEnd],zDerr[xIndStart:xIndEnd,yIndStart:yIndEnd],poly_x,poly_y)
            pFitD,pFitG = p2.polyfit_2d_free_energy(x[xInd],y[yInd],x[ix],y[iy],
                                             zD[xIndStart:xIndEnd,yIndStart:yIndEnd],
                                             zDerr[xIndStart:xIndEnd,yIndStart:yIndEnd],
                                             zG[xIndStart:xIndEnd,yIndStart:yIndEnd],
                                             zGerr[xIndStart:xIndEnd,yIndStart:yIndEnd],
                                             poly_x,poly_y,
                                             startD,startG)
                                             
            print('start',startD)
            print('finish',pFitD)
            yGalD[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFitD)
            pFitGradD_x = p2.grad_poly_2d(pFitD)[0]
            pFitGradD_y = p2.grad_poly_2d(pFitD)[1]
            yGalDerivD_x[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFitGradD_x)
            yGalDerivD_y[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFitGradD_y)
            
            yGalG[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFitG)
            pFitGradG_x = p2.grad_poly_2d(pFitG)[0]
            pFitGradG_y = p2.grad_poly_2d(pFitG)[1]
            yGalDerivG_x[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFitGradG_x)
            yGalDerivG_y[ix,iy] = p2.evaluate_poly_2d(x[ix],y[iy],pFitGradG_y)
    return yGalD,yGalDerivD_x,yGalDerivD_y,yGalG,yGalDerivG_x,yGalDerivG_y

