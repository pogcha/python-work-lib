import numpy as np
import matplotlib.pyplot as plt

def read_kpath_fbands(filename='fbands.dat',n_path=None,n_k=None,n_bands=None):
    if ((n_path is None and n_k is None) 
        or (n_path is None and n_bands is None) 
        or (n_k is None and n_bands is None)):
        raise Exception('Two of n_path, n_k, n_bands are not specified!')
        
    data = np.genfromtxt(filename)
    
    num_rows = data.shape[0]
    
    if n_path is not None and n_k is not None and n_bands is not None:
        if num_rows != n_k*n_bands*n_path:    
            raise Exception('warn')
    
    
    if n_k is None:
        n_k = int(num_rows/(n_bands*n_path))
    elif n_bands is None:
        n_bands = int(num_rows/(n_k*n_path))
    elif n_path is None:
        n_path = int(num_rows/(n_k*n_bands))
    
    data = data.reshape((n_k*n_path,n_bands,2),order='F')
    
    k = data[:,0,0]
    
    bands = data[:,:,1]
    
    return k, bands

def plot_kpath_fbands(k,bands,n_k=None,n_path=None,path_labels=None,plot_fermi=True):
    
    if n_k is None and n_path is None:
        raise Exception('both of n_k and n_path are not specified!')
    
    n_bands = bands.shape[1]
    
    if n_k is None:
        n_k = int(bands.shape[0]/n_path)
    elif n_path is None:
        n_path = int(bands.shape[0]/n_k)
    else:
        assert n_k*n_path == bands.shape[0], 'n_k, n_path and number of k points in bands are inconsistent. Try setting n_k or n_path to None.'
        
    k_reduced = []
    k_reduced.extend(k[0*n_k:(0+1)*n_k])
    for iP in range(1,n_path):
        if k[iP*n_k-1] == k[iP*n_k]:
            k_reduced.extend(k[iP*n_k:(iP+1)*n_k])
        else:
            k_reduced.extend(k[iP*n_k:(iP+1)*n_k]-k[iP*n_k]+k_reduced[-1])

    for iO in range(n_bands):
        plt.plot(k_reduced,bands[:,iO],'-r')
    if plot_fermi:
        plt.plot([k_reduced[0],k_reduced[-1]],[0.0,0.0],':k')
    if path_labels is not None:
        assert len(path_labels) == bands.shape[0]//n_k
        path_labels_reduced = [path_labels[0][0]]
        for iP in range(len(path_labels)-1):
            #print 
            if path_labels[iP][1] == path_labels[iP+1][0]:
                path_labels_reduced.append(path_labels[iP][1])
            else:
                path_labels_reduced.append(path_labels[iP][1]+'|'+path_labels[iP+1][0])
        path_labels_reduced.append(path_labels[-1][1])
    else:
        path_labels_reduced = ''
    x_tick_pos = [k_reduced[ii] for ii in range(0,n_path*n_k,n_k)]
    x_tick_pos.append(k_reduced[-1])   
    plt.xticks(x_tick_pos,path_labels_reduced)
    plt.xlim(k_reduced[0],k_reduced[-1])
    plt.ylabel(r'$E$(eV)')
    plt.xlabel(r'$k$')
    
def readDOS(filename):
    print( 'reading total DOS from file',filename)
    dosData = np.genfromtxt(filename)
    energy = dosData[:,0]
    dos = dosData[:,1:-1]
    print(dos.shape)
    if dos.shape[1] > 1:
        dos = {'up':dos[:,0], 'dn':dos[:,1]}
    return energy, dos
    
def readpDOS(dirname,atoms,smear):
    print('reading partial DOSs for atoms:', atoms,'in dir',dirname)
    #
    if smear:
        adder = '_sm'
    else:
        adder = ''
    fileString =  dirname+'dosp.001{0:}.dat'.format(adder)
    dosData0 = np.genfromtxt(fileString)
    energy = dosData0[:,0]
    numEnergy = dosData0.shape[0]
    numColumns = dosData0.shape[1]
    dosData = np.zeros(shape=(numEnergy,len(atoms),numColumns))
    count = 0
    for at in atoms:
        fileString =  dirname+'dosp.{0:03d}{1:}.dat'.format(at,adder)
        print('reading', dirname+'dosp.{0:03d}{1:}.dat'.format(at,adder))
        dosData[:,count,:] = np.genfromtxt(fileString)
        count += 1
    pDOS = dosData[:,:,1:]
    print(pDOS.shape)
    if pDOS.shape[2] > 9:
        pDOS = {'up':pDOS[:,:,0::2], 'dn':pDOS[:,:,1::2]}

    return energy, pDOS