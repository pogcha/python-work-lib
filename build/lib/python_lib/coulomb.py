import numpy as np
#import scipy.misc as misc
from math import factorial


class matrices:
    def __init__(self):
        # clebsh gordon coefficients for l=2, s=1/2
        # from pdg.lbl.gov/2002/clebrpp.pdf
    
        # first index is j,mj [5/2 5/2;5/2 3/2;3/2 3/2;5/2 1/2;3/2 1/2;5/2 -1/2;3/2 -1/2;5/2 -3/2;3/2 -3/2;5/2 -5/2]
        # second index is ml,ms [-2 1/2; -1 1/2; 0 1/2; 1 1/2; 2 1/2; -2 -1/2; -1 -1/2; 0 -1/2; 1 -1/2; 2 -1/2]
        self.jVec = np.array([5,5,3,5,3,5,3,5,3,5])/2.0
        self.mjVec = np.array([5,3,3,1,1,-1,-1,-3,-3,-5])/2.0
        self.mlVec = np.array([-2,-1,0,1,2,-2,-1,0,1,2])
        self.msVec = np.array([1,1,1,1,1,-1,-1,-1,-1,-1])/2.0
        self.clebMat = np.zeros((10,10))
        
        # 5/2 5/2
        self.clebMat[0,4] = 1.0
        # 5/2 3/2
        self.clebMat[1,9] = 1.0/5.0
        self.clebMat[1,3] = 4.0/5.0
        # 3/2 3/2
        self.clebMat[2,9] = 4.0/5.0
        self.clebMat[2,3] =-1.0/5.0
        # 5/2 1/2
        self.clebMat[3,8] = 2.0/5.0
        self.clebMat[3,2] = 3.0/5.0
        # 3/2 1/2
        self.clebMat[4,8] = 3.0/5.0
        self.clebMat[4,2] =-2.0/5.0
        # 5/2 -1/2
        self.clebMat[5,7] = 3.0/5.0
        self.clebMat[5,1] = 2.0/5.0
        # 3/2 -1/2
        self.clebMat[6,7] = 2.0/5.0
        self.clebMat[6,1] =-3.0/5.0
        # 5/2 -3/2
        self.clebMat[7,6] = 4.0/5.0
        self.clebMat[7,0] = 1.0/5.0
        # ;
        self.clebMat[8,6] = 1.0/5.0
        self.clebMat[8,0] =-4.0/5.0
        # 5/2 -5/2
        self.clebMat[9,5] = 1.0
        
        self.clebMat[self.clebMat>0] = np.sqrt(self.clebMat[self.clebMat>0])
        self.clebMat[self.clebMat<0] = -np.sqrt(np.abs(self.clebMat[self.clebMat<0]))
        
        
        self.rotMat = np.zeros(shape=(5,5),dtype=complex)
        sq2 = 1/np.sqrt(2)
        self.rotMat[0,0] = 1j*sq2
        self.rotMat[0,4] =-1j*sq2
        self.rotMat[1,1] = 1j*sq2
        self.rotMat[1,3] = 1j*sq2
        self.rotMat[2,2] = 1
        self.rotMat[3,1] = sq2
        self.rotMat[3,3] =-sq2
        self.rotMat[4,0] = sq2
        self.rotMat[4,4] = sq2
        #self.rotMat = np.eye(5,dtype=complex)


def lzMatrixWrapper(NimpOrbs):
    # sz

    ll = (NimpOrbs-1)/2
    lz = np.zeros((NimpOrbs,NimpOrbs))
    lp = np.zeros((NimpOrbs,NimpOrbs))
    lm = np.zeros((NimpOrbs,NimpOrbs))
    
    for iO in range(NimpOrbs):
        
        m = -ll+iO
        
        lz[iO,iO] = m
    for iO in range(NimpOrbs-1):
        m = -ll+iO
        matEle = np.sqrt((ll-m)*(ll+m+1))
        lp[iO+1,iO] = matEle
    for iO in range(1,NimpOrbs):
        m = -ll+iO
        matEle = np.sqrt((ll+m)*(ll-m+1))
        lm[iO-1,iO] = matEle
      
    # rotate into cubic basis
    T = matrices().rotMat
    lzR = np.zeros(lz.shape,dtype=complex)
    lpR = np.zeros(lz.shape,dtype=complex)
    lmR = np.zeros(lz.shape,dtype=complex)
    lzR = np.einsum("ij,jo,op",np.conj(T),lz,np.transpose(T))
    lpR = np.einsum("ij,jo,op",np.conj(T),lp,np.transpose(T))
    lmR = np.einsum("ij,jo,op",np.conj(T),lm,np.transpose(T))
            
    return lzR,lpR,lmR

def lsCouplingL2Spherical():
    
    cMat = matrices().clebMat
    aMat = np.linalg.inv(cMat)
    
    jVec = matrices().jVec
    
    # transform fom |j mj> to |ml ms>
    jMat = np.zeros((10,10))
    for i1 in range(10):
        for i2 in range(10):
            # combined sum over j and mj
            for jj in range(10):
                jMat[i1,i2] += jVec[jj]*(jVec[jj]+1.0) * aMat[i1,jj] * aMat[i2,jj]
                
    
    lMat = 6.0*np.eye(10)
    sMat = 3.0/4.0*np.eye(10)
    
    lsMat = 0.5*(jMat - lMat - sMat)
    lsMat[np.abs(lsMat)<1e-14] = 0    
    
    return lsMat
    
    
def lsCouplingL2Cubic():
    
    lsMatSpher = lsCouplingL2Spherical()
    rotMat = matrices().rotMat
    rotMatSpin = np.zeros((10,10),dtype=complex)
    rotMatSpin[:5,:5] = rotMat
    rotMatSpin[5:10,5:10] = rotMat
    #lsMatCub = np.dot(np.dot(rotMatSpin,lsMatSpher),rotMatSpin.conjugate().transpose())
    #print np.all(np.abs(lsMatCub - lsMatCub.transpose().conjugate())<1e-14)
    
    lsMatCub  =np.zeros(shape=(10,10),dtype=complex)
    for ii in range(10):
        for jj in range(10):
            dum = np.tensordot(np.conj(rotMatSpin[ii,:]),lsMatSpher,axes=(0,0))
            lsMatCub[ii,jj] = np.tensordot(dum,rotMatSpin[jj,:],axes=(0,0))
    #print np.all(np.abs(lsMatCub - lsMatCub.transpose().conjugate())<1e-14)
    #print lsMatCub2 - lsMatCub
    #a=b
                    
    return lsMatCub


# some routines to calculate the clebsh-gordon-coeffs
def Jplus(j,m):
    return np.sqrt(j*(j+1) - m*(m+1))
def Jminus(j,m):
    return np.sqrt(j*(j+1) - m*(m-1))
     
def CG(j1,m1,j2,m2,J,M):
  # this is an adaption from a code of Ragnar Stroberg
  if (m1+m2) != M or abs(m1)>j1 or abs(m2)>j2 or abs(M)>J:
      return 0
  if j1>j2+J or j1<abs(j2-J):
      return 0
  # Step 1:  Find all overlaps < (j1, k1)(j2,J-k1) | JJ >
  # They are related by applying J+ to the j1 and j2 components
  # and setting the sum equal to J+|JJ> = 0. Normalization is from completeness.
  # Start with < (j1,j1)(j2,J-j1) | JJ >, with weight 1
  n = 1
  Norm = [1]
  K = np.arange(j1,J-j2-1,-1)    
  for k in K[1:]:
      n *= -Jplus(j2,J-k-1) / Jplus(j1,k)
      Norm.append(n)       
  Norm /= np.sqrt(np.sum(np.array(Norm)**2))
  # Step 2: Apply J- to get from |JJ> to |JM>, and do the same
  # with j1 and j2. Do this for all the overlaps found in Step 1
  cg = 0
  for i,k1 in enumerate(K):
      k2 = J-k1
      if k1<m1 or k2<m2: continue
      # multiply by a factor F to account for all the ways
      # you can get there by applying the lowering operator
      F = factorial(k1-m1+k2-m2) / ( factorial(k1-m1)*factorial(k2-m2) )
      # Apply the lowering operators
      c1,c2,C = 1,1,1
      for k in np.arange(k1,m1,-1):
          c1 *= Jminus(j1,k)
      for k in np.arange(k2,m2,-1):
          c2 *= Jminus(j2,k)
      for k in np.arange(J,M,-1):
          C  *= Jminus(J,k)
      cg += c1*c2/C * Norm[i] * F       
  return cg
  
def aFak(k, l, m1, m2, m3, m4):
   a = 0.

   for q in range(-k,k+1):
       a += (CG(l, m3, k, q, l, m1)*
             CG(l, m2, k, q, l, m4))

   a = a * CG(l,0, k,0, l,0 )**2
   return a

def transform_umatrix(u_mat,rotMat):
    norbitals = u_mat.shape[0]
    uuTrans = np.zeros(shape=(norbitals,norbitals,norbitals,norbitals),dtype=complex)
    for b0 in range(norbitals):
       for b1 in range(norbitals):
           for b2 in range(norbitals):
               for b3 in range(norbitals):
                  dum1 = np.tensordot(np.conj(rotMat[b0,:]),u_mat,axes=(0,0))
                  dum2 = np.tensordot(np.conj(rotMat[b1,:]),dum1,axes=(0,0))
                  dum3 = np.tensordot(dum2,rotMat[b2,:],axes=(0,0))
                  uuTrans[b0,b1,b2,b3] = np.tensordot(dum3,rotMat[b3,:],axes=(0,0))
    return uuTrans
    
def get_spherical2cubic_trans(norbitals):
    # transformation matrix from spherical harmonics to cubic harmonics
    rotMat = np.zeros(shape=(norbitals,norbitals),dtype=complex)
    
    if np.sum(norbitals == np.array([1,3,5,7])) == 0:
       raise NotImplementedError('Coulomb only implemented for norbitals = 1, 3, 5, 7, i.e., s, p, d, f orbitals')
    if norbitals == 7:
       # order of sphericals: m=-3,-2,-1,0,1,2,3
       # order of cubics: x(x^2-3y^2), z(x^2-y^2), xz^2, z^3, z^3, yz^2, xyz,  y(3x^2 - y^2)
       sq2 = 1/np.sqrt(2)
       rotMat[0,0] = sq2
       rotMat[0,6] =-sq2
       rotMat[1,1] = sq2
       rotMat[1,5] = sq2
       rotMat[2,2] = sq2
       rotMat[2,4] =-sq2
       rotMat[3,3] = 1.0
       rotMat[4,2] = 1j*sq2
       rotMat[4,4] = 1j*sq2
       rotMat[5,1] = 1j*sq2
       rotMat[5,5] =-1j*sq2
       rotMat[6,0] = 1j*sq2
       rotMat[6,6] = 1j*sq2   
           
    elif norbitals == 5:
       # order of sphericals: m=-2, -1, 0, 1, 2
       # order of cubics: xy, yz, z^2, xz, x^2-y^2 (like in VASP)
       sq2 = 1/np.sqrt(2)
       rotMat[0,0] = 1j*sq2;
       rotMat[0,4] =-1j*sq2
       rotMat[1,1] = 1j*sq2
       rotMat[1,3] = 1j*sq2
       rotMat[2,2] = 1.0
       rotMat[3,1] = sq2
       rotMat[3,3] =-sq2
       rotMat[4,0] = sq2
       rotMat[4,4] = sq2
    elif norbitals == 3:
       # order of sphericals: m=1,0,-1
       # order of cubics: y, z, x
       sq2 = 1.0/np.sqrt(2)
       rotMat[0,0] = 1j*sq2
       rotMat[0,2] = 1j*sq2
       rotMat[1,1] = 1.0
       rotMat[2,0] = sq2
       rotMat[2,2] = -sq2
    elif norbitals == 1:
       rotMat[0,0] = 1
    return rotMat
   

def set_u_matrix(norbitals,slater,basis='cubic'):
   """Initializing Coulomb interaction from the F0 ... F6 Slater parameters."""
   if (basis != 'spherical') and (basis != 'cubic'):
       raise NotImplementedError('Coulomb only implemented for cubic and spherical basis')
   if np.sum(norbitals == np.array([1,3,5,7])) == 0:
       raise NotImplementedError('Coulomb only implemented for norbitals = 1, 3, 5, 7, i.e., s, p, d, f orbitals')
   #if norbitals != 5:
   #    raise NotImplementedError('Coulomb only implemented for complete d-shells (5 orbitals)')
   l = (norbitals-1)//2

   uuSph = np.zeros(shape=(norbitals,norbitals,norbitals,norbitals))
   mList = range(-l,l+1)
   for m1 in mList:
       for m2 in mList:
           for m3 in mList:
               for m4 in mList:
                   # +l to fix the indexing   
                   for kk in range(l+1):
                       uuSph[m1+l,m2+l,m3+l,m4+l] += (
                            slater[kk] * aFak(2*kk,l,m1,m2,m3,m4)
                                            )
                                            
   if basis == 'spherical':
       return uuSph
   elif basis == 'cubic':
       
       rotMat = get_spherical2cubic_trans(norbitals)        
       uuCub = transform_umatrix(uuSph,rotMat)
       return uuCub


  
