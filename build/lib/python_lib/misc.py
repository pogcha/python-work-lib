# -*- coding: utf-8 -*-
"""
Created on Tue Jul 17 10:46:00 2018

@author: mschueler
"""

import time
import numpy as np
class progress():
    
    def __init__(self):
        self.timer_running = False
        
    def start_timer(self):
        self.start_time = time.time()
        self.timer_running = True
        
    def print_progress(self,counter,total):
        
        if self.timer_running == False:
            self.start_timer()
            print('{0:3.2f}% ttg -'.format(0.0))
            
        sofar = time.time()-self.start_time
        amount_done = (counter+0.001)/total
        amount_togo = 1.0-amount_done
        ttg = sofar/amount_done*amount_togo
        d = int(ttg/3600.0/24.0)
        h = int(ttg/3600.0) - d*24
        m = int(ttg/60.0) - h*60 - d*24*60
        s = int(ttg) - h*60*60 - d*24*60*60 - m*60
        timeStr = 'ttg'
        if d > 0:
            timeStr = timeStr + ' {0:d}d'.format(d)
        if h > 0:
            timeStr = timeStr + ' {0:d}h'.format(h)
        if m > 0:
            timeStr = timeStr + ' {0:d}min'.format(m)
        timeStr = timeStr + ' {0:d}s'.format(s)

        print('{0:3.2f}% {1:}'.format(100.0*amount_done,timeStr))

def wavelength_to_rgb(wavelength, gamma=0.8):

    '''This converts a given wavelength of light to an 
    approximate RGB color value. The wavelength must be given
    in nanometers in the range from 380 nm through 750 nm
    (789 THz through 400 THz).

    Based on code by Dan Bruton
    http://www.physics.sfasu.edu/astro/color/spectra.html
    '''

    wavelength = float(wavelength)
    if wavelength >= 380 and wavelength <= 440:
        attenuation = 0.3 + 0.7 * (wavelength - 380) / (440 - 380)
        R = ((-(wavelength - 440) / (440 - 380)) * attenuation) ** gamma
        G = 0.0
        B = (1.0 * attenuation) ** gamma
    elif wavelength >= 440 and wavelength <= 490:
        R = 0.0
        G = ((wavelength - 440) / (490 - 440)) ** gamma
        B = 1.0
    elif wavelength >= 490 and wavelength <= 510:
        R = 0.0
        G = 1.0
        B = (-(wavelength - 510) / (510 - 490)) ** gamma
    elif wavelength >= 510 and wavelength <= 580:
        R = ((wavelength - 510) / (580 - 510)) ** gamma
        G = 1.0
        B = 0.0
    elif wavelength >= 580 and wavelength <= 645:
        R = 1.0
        G = (-(wavelength - 645) / (645 - 580)) ** gamma
        B = 0.0
    elif wavelength >= 645 and wavelength <= 750:
        attenuation = 0.3 + 0.7 * (750 - wavelength) / (750 - 645)
        R = (1.0 * attenuation) ** gamma
        G = 0.0
        B = 0.0
    else:
        R = 1.0
        G = 1.0
        B = 1.0
    #R *= 255
    #G *= 255
    #B *= 255
    return (R, G, B)          


def d_trans(inv_T):
    # transformation matrix for d-orbitals for a given
    # transformation matrix of (xyz) coordinates
    trans_d = np.zeros((5,5),dtype=float)
    #d-orbitals                                                                                                                      
    #dxy
    trans_d[0,0] = inv_T[0,0]*inv_T[1,1]+inv_T[0,1]*inv_T[1,0]
    trans_d[0,1] = inv_T[0,1]*inv_T[1,2]+inv_T[0,2]*inv_T[1,1]
    trans_d[0,2] = inv_T[0,2]*inv_T[1,2]*np.sqrt(3.)
    trans_d[0,3] = inv_T[0,0]*inv_T[1,2]+inv_T[0,2]*inv_T[1,0]
    trans_d[0,4] = inv_T[0,0]*inv_T[1,0]-inv_T[0,1]*inv_T[1,1]
    #dyz
    trans_d[1,0] = inv_T[1,0]*inv_T[2,1]+inv_T[1,1]*inv_T[2,0]
    trans_d[1,1] = inv_T[1,1]*inv_T[2,2]+inv_T[1,2]*inv_T[2,1]
    trans_d[1,2] = inv_T[1,2]*inv_T[2,2]*np.sqrt(3.)
    trans_d[1,3] = inv_T[1,0]*inv_T[2,2]+inv_T[1,2]*inv_T[2,0]
    trans_d[1,4] = inv_T[1,0]*inv_T[2,0]-inv_T[1,1]*inv_T[2,1]

    #dz2 
    trans_d[2,0] = 2*( -inv_T[0,0]*inv_T[0,1]-inv_T[1,0]*inv_T[1,1]+2*inv_T[2,0]*inv_T[2,1] ) / ( 2*np.sqrt(3.) )
    trans_d[2,1] = 2*( -inv_T[0,1]*inv_T[0,2]-inv_T[1,1]*inv_T[1,2]+2*inv_T[2,1]*inv_T[2,2] ) / ( 2*np.sqrt(3.) )
    trans_d[2,2] =   ( -inv_T[0,2]*inv_T[0,2]-inv_T[1,2]*inv_T[1,2]+2*inv_T[2,2]*inv_T[2,2] ) / ( 2. )
    trans_d[2,3] = 2*( -inv_T[0,0]*inv_T[0,2]-inv_T[1,0]*inv_T[1,2]+2*inv_T[2,0]*inv_T[2,2] ) / ( 2*np.sqrt(3.) )
    trans_d[2,4] = ( (-inv_T[0,0]*inv_T[0,0]-inv_T[1,0]*inv_T[1,0]+2*inv_T[2,0]*inv_T[2,0]) 
                     - (-inv_T[1,1]*inv_T[1,1]-inv_T[0,1]*inv_T[0,1]+2*inv_T[2,1]*inv_T[2,1]) )   / ( 2*np.sqrt(3.) )

    #dxz                                                                      
    trans_d[3,0] = inv_T[0,0]*inv_T[2,1] + inv_T[0,1]*inv_T[2,0]
    trans_d[3,1] = inv_T[0,1]*inv_T[2,2] + inv_T[0,2]*inv_T[2,1]
    trans_d[3,2] = inv_T[0,2]*inv_T[2,2] * np.sqrt(2.)
    trans_d[3,3] = inv_T[0,0]*inv_T[2,2] + inv_T[0,2]*inv_T[2,0]
    trans_d[3,4] = inv_T[0,0]*inv_T[2,0] - inv_T[0,1]*inv_T[2,1]

    #dx2
    trans_d[4,0] = 2*( inv_T[0,0]*inv_T[0,1] - inv_T[1,0]*inv_T[1,1] ) / ( 2. )
    trans_d[4,1] = 2*( inv_T[0,1]*inv_T[0,2] - inv_T[1,1]*inv_T[1,2] ) / ( 2. )
    trans_d[4,2] = ( inv_T[0,2]*inv_T[0,2] - inv_T[1,2]*inv_T[1,2] ) * np.sqrt(3.) / ( 2. )
    trans_d[4,3] = 2*( inv_T[0,0]*inv_T[0,2] - inv_T[1,0]*inv_T[1,2] ) / ( 2. )
    trans_d[4,4] = ( (inv_T[0,0]*inv_T[0,0]-inv_T[1,0]*inv_T[1,0])-(inv_T[0,1]*inv_T[0,1]-inv_T[1,1]*inv_T[1,1]) ) / ( 2. )
    return trans_d  
# test enviroment
           
#prog = progress()

#numRuns = 3630*24
#for ii in range(numRuns):
#    prog.print_progress(ii,numRuns)
#    time.sleep(1.0)