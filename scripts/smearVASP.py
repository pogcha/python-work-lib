#!/usr/bin/env python
# -*- coding: utf-8 -*-

# apply lorentioan smearing to VASP (partial) density of states
# the DOS files are assumed to be output from the vdos script

import numpy as np
import python_lib.smearBib as smearBib
# input stuff

# fix python 2.x behavior to evaluate input. override input with raw_input
if hasattr(__builtins__, 'raw_input'):
    input=raw_input

tp = input("total or partial ('t', 'p') ")
if tp == 'p':
    numAtom = (input("How many atoms to be smeared? (hit enter for default 1)"))
    startAtom = (input("With which atom to start? (hit enter for default 1)"))
    if numAtom == '':
        numAtom = 1
    else:
        numAtom=int(numAtom)
    if startAtom == '':
        startAtom = 1
    else:
        startAtom = int(startAtom)
elif tp == 't':
    numAtom = 1
    startAtom = 1
else:
    print('error: enter \'t\' or \'p\', exiting...')
    exit()
    
sig = (input("Which Smearing to apply? (hit enter for defaut=0.05) sigma="))

if sig == '':
    sig = 0.05
else:
    sig = float(sig)
   
print( 'smearing with sigma=',sig)



for ii in range(startAtom,startAtom+numAtom):
    if tp == 'p':
        filename = 'dosp.{0:003d}.dat'.format(ii)
        savename = 'dosp.{0:003d}_sm.dat'.format(ii)
    elif tp == 't':
        filename = 'dost.dat'
        savename = 'dost_sm.dat'
    print( 'smearing file '+filename)
    
    # input and format stuff
    # forget about first point of data to make the calculation of dE easy
    data = np.genfromtxt(filename)
    energy = data[1:,0]
    if tp == 'p':
        DOS = data[1:,1:]
    elif tp == 't':
        DOS= data[1:,1]
        DOS = np.reshape(DOS,(-1,1))
    nOrbs = DOS.shape[1]
    nEners = energy.size
    
    # calculation stuff
    
    DOSsmear = smearBib.smearMulti(energy,DOS,sig)
    
    # output stuff
    energy = np.reshape(energy,(-1,1))
    output = np.hstack((energy,DOSsmear))
    np.savetxt(savename,output,fmt='%.5f '+'%.5E '*nOrbs)

            
