#!/usr/bin/env python
# -*- coding: utf-8 -*-

# read COORDS file (local coordinate systems per atom)
# and give transformation matrices for d-manifold

from __future__ import print_function
import numpy as np
from python_lib import  misc
import sys


data = np.genfromtxt('COORDS',usecols=[2,3,4])

if len(sys.argv) == 1:
    nAt = data.shape[0]//3
else:
    nAt = int(sys.argv[1])
print(nAt)

for ii in range(nAt):
    xyz = data[ii*3:(ii+1)*3,:]
    print('\nAtom '+str(ii))
    print('xyz coordinate system')
    print(xyz)

    d1 = misc.d_trans(xyz)
    print('d transformation')
    for ii in range(5):
        string = ''
        for jj in range(5):
            string = string + '{0: 1.10f} '.format(d1[ii,jj])
        print(string)

