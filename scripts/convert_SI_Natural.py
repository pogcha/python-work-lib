#! /usr/bin/env python
from __future__ import print_function
from math import pi
import numpy as np
import optparse

# SI definitions of units 
c = 2.99792458e8 #m/s # http://physics.nist.gov/cgi-bin/cuu/Value?c
hbar = 1.054571800e-34 #J/s = m^2 kg /s # http://physics.nist.gov/cgi-bin/cuu/Value?hbar
eV = 1.6021766208e-19 #J = m^2 kg / s^2 # http://physics.nist.gov/cgi-bin/cuu/Value?e
e0 = 1e7/4.0/pi/c**2 #A^2 s^4 kg^-1 m^-2

# stuff for parsing commandline input
parser = optparse.OptionParser(usage = "%prog --unit=[si]/nat -- value dim1 dim2 dim3 dim4 \n"+
                               "   for si:  dimensions: m,s,kg,A\n"+
                               "   for nat: dimensions: c,hbar,eV,4pie0")
parser.add_option("--unit", type="choice",dest='inputUnitSystem', choices=["si","nat"],
                  help="unit system of input value, choices [si]/nat")

(options, args) = parser.parse_args()

if not args:
    raise Exception('please provide input. For help invoke program with -h or --help')

if len(args) != 5:
    print ('Error in input: Not enough input arguments (should be 5)\n exiting...')
    exit()

# convert parsed input to float
inputValue = float(args[0])
try:
    dim = [float(x) for x in args[1:]]
except ValueError as er:
    print ('Error in input: '+str(er)+'\n exiting...')
    exit()


# compare this matrix with the definition of c, hbar, eV, and e0...
matNatToSI = np.array([[1, 0, 0, 0, 0],
                       [np.log10(c), 1, -1, 0, 0],
                       [np.log10(hbar), 2, -1, 1, 0],
                       [np.log10(eV), 2, -2, 1, 0],
                       [np.log10(4.0*pi*e0), -3, 4, -1, 2]]).transpose()


transformArray = np.array([np.log10(inputValue), dim[0], dim[1], dim[2], dim[3]])

# do the transformations
if options.inputUnitSystem == 'si':
    new = np.dot(np.linalg.inv(matNatToSI), transformArray)
elif options.inputUnitSystem == 'nat':
    new = np.dot(matNatToSI, transformArray)

# revert the logarithm and save new dimensions
outputValue = 10**new[0]
outDim = new[1:]

# this is for nice formating of input and outpu
natDims = ['c','hbar','eV','(4pie0)']
siDims = ['m','s','kg','A']


def stringify(dim):
    # decides how power of dimension is printed
    # based on integer / not integer and 
    # one / not one
    if np.abs(np.abs(dim) - 1.0) < 1e-5:
        if dim > 0.0:
            return ''
        else:
            return '^{0:1.0f}'.format(dim)
    if np.abs(dim%1.0) < 1e-5:
        return '^{0:1.0f}'.format(dim)
    else:
        return '^{0:1.1f}'.format(dim)

def appendDim(comString,dimString,dim):
    # appends a string to output dimension string
    # if dimension is not zero. also inserts spaces
    # everywhere but at the begining
    if np.abs(dim) < 1e-3:
        pass
    else:
        if comString != '':
            comString +=' '
        comString += dimString+stringify(dim)
    return comString

natString = ''
siString = ''
if options.inputUnitSystem == 'nat':
    for idim in range(4):
        natString = appendDim(natString,natDims[idim],dim[idim])
        siString = appendDim(siString,siDims[idim],outDim[idim])
if options.inputUnitSystem == 'si':
    for idim in range(4):
        natString = appendDim(natString,natDims[idim],outDim[idim])
        siString = appendDim(siString,siDims[idim],dim[idim])

if options.inputUnitSystem == 'nat':
    dimStringOld = ('{0:1.9e} '+natString).format(inputValue,
                                                  dim[0], dim[1], dim[2], dim[3])
    dimStringNew = ('{0:1.9e} '+siString).format(outputValue,
                                                 outDim[0], outDim[1], outDim[2], outDim[3])
elif options.inputUnitSystem == 'si':
    dimStringOld = ('{0:1.9e} '+siString).format(inputValue,
                                                dim[0],dim[1],dim[2],dim[3])
    dimStringNew = ('{0:1.9e} '+natString).format(outputValue,
                                                  outDim[0], outDim[1], outDim[2], outDim[3])

print(' converted \n   ' + dimStringOld + 
      '\n = ' + dimStringNew + '\n')


