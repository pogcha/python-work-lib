#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import h5py
from optparse import OptionParser


# input stuff
parser = OptionParser(usage="%prog hk-filename [options]")
parser.add_option("-i", "--iter",
                  help="iteration to grab",
                    default='-1',type="int")
parser.add_option("-p", "--plot",
                  action="store_true",  default=False,
                  help="plot the semilogy curves of accept-pair-tau vs. tau")

parser.add_option("-v", "--verbose",
                  action="store_true",  default=False,
                  help="print tauDiffMax for all orbs and spins and not only the maximum value")       
           
# fix python 2.x behavior to evaluate input. override input with raw_input
(options, args) = parser.parse_args()



if len(args) == 0:
    raise Exception('hdf5 filename not provided! provide by \'set_tau_diff_max.py file.h5\'')
else:
    h5file = args[0]

if options.iter<-1:
    raise Exception('iter (-i, --iter) hsa to be positive. Only Exception is -1 to get last iteration.')


if options.iter == -1:
    print('\ngetting last iteration\n')
else:
    print('\ngetting iteration '+str((options.iter))+'\n')

data = []

with h5py.File(h5file, "r") as ar:
    if options.iter == -1:
        link = 'dmft-last'
    else:
        link = 'dmft-{0:03d}'.format(options.iter)
    for group in ar[link]:
        if group[:4] == 'ineq':
            link2 = link+'/'+group+'/accept-pair-tau/value'
            data.append(ar[link2][...])
    tau = ar['.axes/taubin'][...]
    
print('found '+str(len(data))+' atoms\n')

for iA in range(len(data)):

    # get indices where accept-pair-tau is close to 1% of value at tau=0
    indi = []
    if options.verbose:
        print('atom'+str(iA+1)+':')
        print(' orbital spin tauDiffMax  ')
    for iO in range(5):
        for iS in range(2):
            indi.append( np.argmin(np.abs(data[iA][iO,iS,:] - data[iA][iO,iS,0]*0.01)))
            if options.verbose:
                print('  {0:3d}      {1:d}    {2:1.5f}'.format(iO+1,iS+1,tau[indi[-1]]))

    print('atom {0:3d} taudDiffMax  {1:1.5f} (max of orbs and spins)'.format(iA+1,np.max([tau[x] for x in indi])))
    if options.plot:
        for iO in range(5):
            for iS in range(2):
                # plot line at 1% of tau=0 value
                plt.semilogy(tau,np.ones(tau.size)*data[iA][iO,iS,0]*0.01)
                # plot data
                plt.semilogy(tau,data[iA][iO,iS,:])

if options.plot:
    plt.show()
