#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import h5py as hdf5
from optparse import OptionParser


# input stuff
parser = OptionParser(usage="%prog hdf5-filename [options]")
parser.add_option("-p", "--plot",
                  action="store_true",  default=False,
                  help="plot the semilogy curves of accept-pair-tau vs. tau")
       
           
# fix python 2.x behavior to evaluate input. override input with raw_input
(options, args) = parser.parse_args()



if len(args) == 0:
    raise Exception('hdf5 filename not provided! provide by \'set_tau_diff_max.py file.h5\'')
else:
    h5file = args[0]

hf = hdf5.File(h5file, "r+")
iternodes = []
for key in hf.keys():
    #print(key)                                                                                        
    if ('dmft' in key) and (('last' in key) == False):
        iternodes.append(hf[key])

iw = hf['.axes']['iw'].value
mu = []
occ = []
siw = []
for iter in iternodes:
    mu.append(iter["mu/value"].value)
    occDummy = []
    siwDummy = []
    for ineq in iter.keys():
        if ineq.startswith('ineq-'):
            occDummy.append(iter[ineq]['occ/value'].value)
            siwDummy.append(iter[ineq]['siw/value'].value)
            #for onj in iter[ineq]:                                                                    
            # if you wanna print all objects                                                           
            #    print(onj)                                                                            

    occ.append(occDummy)
    siw.append(siwDummy)
    
    
siw = np.array(siw)
occ = np.array(occ)
occDiag = np.diagonal(occ,axis1=2,axis2=4)
occDiag = np.diagonal(occDiag,axis1=2,axis2=3)

print()
print('chemical potential')
print(np.array(mu))
print()

print('impurity occupation, atomwise')
for iA in range(occ.shape[1]):
    print(np.sum(occDiag[:,iA,:,:],axis=(1,2)))
print()



print('impurity magnetization, atomwise')
for iA in range(occ.shape[1]):
    #print 'impurity magnetization, atom'+str(iA+1)                                                    
    print(np.sum(occDiag[:,iA,:,0] - occDiag[:,iA,:,1],axis=1))
print()

print('Im[Sigma(iw_n=0)] atomwise, orbital and spin with largest value')
for iA in range(occ.shape[1]):
    for iI in range(siw.shape[0]):
        print('{0:1.4f}'.format(np.min(siw[iI,iA,:,:,siw.shape[-1]/2].imag,axis=(0,1))),end=" ")
    print()
print()   

if options.plot:
    siw = siw[...,0]
    siw -= siw[-1,...]
    
    plt.figure(1)
    plt.subplot(3,1,1)
    print(occDiag.shape)
    for ii in range(occDiag.shape[1]):
        plt.plot(np.sum(occDiag[:,ii,:,:],axis=(1,2)))
        #plt.errorbar(range(occDiag.shape[0]),occDiag[:,ii,0],occDiag[:,ii,1])                             
    plt.ylabel('n_imp')
    plt.subplot(3,1,2)
    plt.plot(mu)
    plt.ylabel('mu')
    plt.subplot(3,1,3)
    for ii in range(siw.shape[1]):
        for jj in range(siw.shape[2]):
            for kk in range(siw.shape[3]):
                if ii == 0 and jj == 0 and kk == 0 or True:
                    plt.plot(siw[:,ii,jj,kk].real)
    plt.ylabel('real(Siw(0))')
    plt.show()
