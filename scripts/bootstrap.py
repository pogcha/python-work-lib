#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 13:01:17 2018

@author: mschueler
"""


from __future__ import print_function
import numpy as np

import h5py


from optparse import OptionParser

# input stuff
parser = OptionParser(usage="%prog hk-filename [options]")
parser.add_option("-i", "--iter",
                  help="iteration to grab",
                    default='-1',type="int")
parser.add_option("-p", "--plot",
                  action="store_true",  default=False,
                  help="plot gtau and the relative error")
parser.add_option("-b", "--box",
                  help="size of box where fit is performed. Has to be even",
                    default='16',type="int")
parser.add_option("-f", "--forder",
                  help="order of polynomial fit to perform in the box",
                    default='3',type="int")
parser.add_option("-a", "--atom",
                  help="for which atom to inspect gtau in the plot",
                    default='0',type="int")
parser.add_option("-s", "--spin",
                  help="for which spin to inspect gtau in the plot",
                    default='0',type="int")
parser.add_option("-o", "--orb",
                  help="for which orbital to inspect gtau in the plot",
                    default='0',type="int")
parser.add_option("-n", "--numboot",
                  help="number of bootstrap steps to perform",
                    default='5',type="int")



# fix python 2.x behavior to evaluate input. override input with raw_input
(options, args) = parser.parse_args()

box  = options.box
fitorder = options.forder
inspectSpin = options.spin
inspectAtom = options.atom
inspectOrb = options.orb
numBoot = options.numboot

if box%2 != 0:
    raise Exception('box (-b, --box) has to be even')

if options.iter<-1:
    raise Exception('iter (-i, --iter) hsa to be positive. Only Exception is -1 to get last iteration.')

if len(args) == 0:
    raise Exception('hdf5 filename not provided! provide by \'bootstrap.py file.hdf5\'')
else:
    hFile = args[0]


if options.iter == -1:
    print('\nGetting last iteration')
else:
    print('\nGetting Iteration number {0:d}'.format(options.iter))   

print('\ndoing bootstrap for an error estimate of gtau:')
print('fit box:                   {0:d}'.format(box))
print('fit order:                 {0:d}'.format(fitorder))
print('number of bootstrap steps: {0:d}'.format(numBoot))



if options.plot:
    print('\nplotting results for')
    print('atom:    {0:d}'.format(inspectAtom))
    print('orbital: {0:d}'.format(inspectOrb))
    print('spin:    {0:d}'.format(inspectSpin))
else:
    print('\nnot plotting results')


if options.plot:
    import matplotlib.pyplot as plt

gtau = []
gtauErr = []
with h5py.File(hFile,"r") as hf:
    if options.iter == -1:
        link = 'dmft-last'
    else:
        link = 'dmft-{0:03d}'.format(options.iter)
    for group in hf[link]:
        if group[:4] == 'ineq':
            linkData = link+'/'+group+'/gtau/value'
            linkError = link+'/'+group+'/gtau/error'
            gtau.append(hf[linkData][...])
            gtauErr.append(hf[linkError][...])             
    tau = hf['.axes/tau'][...]

print('\nfound {0:d} atoms\n'.format(len(gtau)))

gtau = np.array(gtau)
gtauErr = np.array(gtauErr)
gtauErrBoot = np.zeros(gtauErr.shape)

for itau in range(tau.size):
    if itau%10 == 0:
        print('itau {0:d} of {1:d}'.format(itau,tau.size))
    if itau-box//2 < 0:
        boxInd = [0,box]
    elif itau+box//2 > tau.size-1:
        boxInd = [tau.size-box,tau.size]
    else:
        boxInd = [itau-box//2,itau+box//2]

    tauBox = tau[boxInd[0]:boxInd[1]]
    gtauBox = gtau[:,:,:,boxInd[0]:boxInd[1]]


    model = np.zeros((gtau.shape[0],gtau.shape[1],gtau.shape[2],box))
    for iA in range(gtau.shape[0]):
        for iO in range(gtau.shape[1]):
            for iS in range(gtau.shape[2]):
                fit = np.polyfit(tauBox,gtauBox[iA,iO,iS,:],fitorder)
                model[iA,iO,iS,:] = np.polyval(fit,tauBox)
    
    # do the bootstrap:
    means = np.zeros((numBoot,gtau.shape[0],gtau.shape[1],gtau.shape[2]))
    for ii in range(numBoot):
        choose = np.random.randint(0,box,box)
        diff = np.abs(model[...,choose] - gtauBox[...,choose])
        means[ii,...] = np.mean(diff,axis=-1)
    gtauErrBoot[...,itau] = np.mean(means,axis=0)
  
factor = np.mean(gtauErrBoot/gtauErr,axis=-1)

spins = ['up  ','down']
print('rescaling factors:')
for iA in range(gtau.shape[0]):
    for iO in range(gtau.shape[1]):
        for iS in range(gtau.shape[2]):
            print(" atom {0:d} orb {1:d} spin {2:} {3:1.3f}".format(iA+1,iO+1,spins[iS],factor[iA,iO,iS]))

print('  max factor: {0:1.3f}'.format(np.min(factor)))
print('  min factor: {0:1.3f}'.format(np.max(factor)))


if options.plot:

    plt.figure(1)
    plt.suptitle('atom '+str(inspectAtom)+' orbital '+str(inspectOrb)+' spin '+str(inspectSpin))

    plt.subplot(2,1,1)

    plt.plot(tau,gtau[inspectAtom,inspectOrb,inspectSpin,:],'-')
    plt.errorbar(x=tau,y=gtau[inspectAtom,inspectOrb,inspectSpin,:],
                 yerr=gtauErrBoot[inspectAtom,inspectOrb,inspectSpin,:],linestyle='')
    plt.errorbar(x=tau,y=gtau[inspectAtom,inspectOrb,inspectSpin,:],
                 yerr=gtauErr[inspectAtom,inspectOrb,inspectSpin,:],linestyle='')
    plt.ylabel('gtau')

    plt.subplot(2,1,2) 
    
    plt.plot(tau,gtauErrBoot[inspectAtom,inspectOrb,inspectSpin,:]
                 /gtau[inspectAtom,inspectOrb,inspectSpin,:],label='bootstraped')
    plt.plot(tau,gtauErr[inspectAtom,inspectOrb,inspectSpin,:]
                 /gtau[inspectAtom,inspectOrb,inspectSpin,:],label='original')
    plt.plot(tau,factor[inspectAtom,inspectOrb,inspectSpin]
                  *gtauErr[inspectAtom,inspectOrb,inspectSpin,:]
                  /gtau[inspectAtom,inspectOrb,inspectSpin,:],label='orig. rescaled')
    plt.xlabel('tau')
    plt.ylabel('relative error')
    plt.legend(frameon=False)
    
    plt.show()      

