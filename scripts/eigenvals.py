#!/usr/bin/env python
# -*- coding: utf-8 -*-

# calculate eigenvalues of hamiltonian and save to file

from __future__ import print_function
import numpy as np
import python_lib.hk as hk
from optparse import OptionParser
parser = OptionParser(usage="%prog hk-filename [options]")

parser.add_option("-v", "--version",
                  help="header version of H(k)",
                    default='3',type="int")
(options, args) = parser.parse_args()

if len(args) == 0:
    raise Exception('H(k) filename not provided! provide by \'eigenvals.py file.dat\'')
else:
    hk_filename = args[0]

ham = hk.readHk(hk_filename,options.version)[0]
eigVals,eigVecs = np.linalg.eigh(ham)

np.savetxt("eigenvals.dat", eigVals)
