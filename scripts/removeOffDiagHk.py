#!/usr/bin/env python
# -*- coding: utf-8 -*-

# subtract the local hamiltonian from H(k) on the offdiagonals to 
# obtain a digonal local hamiltonian

from __future__ import print_function
import python_lib.hk as hk
from optparse import OptionParser
import numpy as np

# input stuff
parser = OptionParser(usage="%prog hk-filename [options]")
parser.add_option("-v", "--version",
                  help="header version of H(k)",
                    default='3',type="int")

# fix python 2.x behavior to evaluate input. override input with raw_input
(options, args) = parser.parse_args()

if len(args) == 0:
    raise Exception('H(k) filename not provided! provide by \'dosHk.py file.dat\'')
else:
    hk_filename = args[0]

print('')
ham, nD, nP, nAt, kVec, nE  = hk.readHk(hk_filename,options.version)
print('')

nK = ham.shape[0]
print('subtracting local hamiltonian from H(k)...\n')
# local hamiltonian:
ham_loc = np.sum(ham,axis=0)/nK
# subtract the local hamiltonian and add back the diagonal
ham_diag = np.zeros(shape=ham.shape,dtype=complex)
for iK in range(nK):
    ham_diag[iK,:,:] = np.diag(np.diag(ham[iK,:,:]))

# subtracting the local hamiltonian on the offdiagonals leads to a diagonal
# local Hamiltonian but not to diagonal local green functions
#ham_diag = ham - ham_loc[np.newaxis,:,:] + np.diag(np.diag(ham_loc))

# test for local greens function
#import python_lib.greens_functions as greens
#z = np.array([1j*1.0])
#mu = 2.0
#Hk = ham_diag
#out, _, _ = greens.non_interacting_greens_vecK(z,mu,Hk,diagonal=False,cpu_count=4)
#print(out)
hk.writeHk(hk_filename+'_diag',options.version,ham_diag,kVec,nAt,nD,nP,nE)



