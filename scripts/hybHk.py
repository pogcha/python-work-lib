#!/usr/bin/env python
# -*- coding: utf-8 -*-

# apply lorentioan smearing to VASP (partial) density of states
# the DOS files are assumed to be output from the vdos script

from __future__ import print_function
import numpy as np
import python_lib.greens_functions as green
import python_lib.hk as hk
from optparse import OptionParser
from optparse import OptionGroup
import matplotlib.pyplot as plt
import os


# input stuff
parser = OptionParser(usage="%prog hk-filename [options]")
group_w = OptionGroup(parser, "Energy axis options")
group_s = OptionGroup(parser, "Save options")
parser.add_option("-v", "--version",
                  help="header version of H(k)",
                    default='3',type="int")
parser.add_option("-o", "--orbitals",
                  help="number of orbitals for which Delta is calculated",
                    default=1,type="int")
parser.add_option("-c", "--ncpu",
                  help="number of cpus to use. Keep in mind that numpy often comes with implicit parallization. It may be faster to set ncpu = 1 or OMP_NUM_THREADS=1",
                    default=None,type='int')

group_w.add_option("-m", "--mu",
                  help="chemical potential",
                    default='0.0',type="float")
group_w.add_option("-i", "--idelta",
                  help="broadening",
                    default='0.1',type="float")
group_w.add_option("-l", "--wmin",
                  help="lower boundary of energy energy-axis",
                    default=None,type="float")
group_w.add_option("-u", "--wmax",
                  help="upper boundary of energy energy-axis",
                    default=None,type="float")
group_w.add_option("-n", "--nw",
                  help="number of energy points",
                    default=None,type="int")

group_w.add_option("-b", "--beta",
                  help="inverse temperature (only used for matsubara calculation",
                    default=None,type="float")
group_w.add_option("--matsub",action="store_true",
                  help="switch to calculation in matsubara freq.",
                    dest="matsubara",default=False)
group_s.add_option("--offdiag",action="store_true",
                  help="save Delta as a matrix instead of only the diagonals.",
                    dest="offdiag",default=False)
group_s.add_option("-a","--archive",action="store_true",
                  help="save all output to hdf5 file additionally.",
                    dest="archive",default=False)
group_s.add_option("-s", "--savefigure",action="store_true",
                  help="save the plot instead of showing it",
                    default=False)
parser.add_option_group(group_w)
parser.add_option_group(group_s)
# fix python 2.x behavior to evaluate input. override input with raw_input
(options, args) = parser.parse_args()


# reading partial_selec.dat
num_par = 0
if os.path.isfile('orb_selec.dat') :
    with open('orb_selec.dat') as f:
        lines = f.readlines()
        for ll in lines:
            if lines[0] == '#':
                pass
            else:
                par = [int(x) for x in ll.split()]

    num_par = len(par)
    print('\nfound file orb_selec.dat')
    print('defined for which orbitals to calculate Delta:')
    for ii in range(num_par):
        print('   '+str(ii)+': '+str(par[ii]))
    if num_par != options.orbitals:
        print('')
        print('WARNING')
        print('this overrides the --orbitals option!')
        print("")
    else:
        print('this overrides the --orbitals option!')

else:
    print('\ndid not find file orb_selec.dat: using the first few orbitals.')
    print('to calculate Delta. To select different orbitals, generate a file')
    print('called orb_selec.dat and populate it with a single line with')
    print('the desired orbitals, e.g., ')
    print('\n0 2 3')
    par = [x for x in range(options.orbitals)]
    num_par = len(par)
    
    
if len(args) == 0:
    raise Exception('H(k) filename not provided! provide by \'dosHk.py file.dat\'')
else:
    hk_filename = args[0]


print('')
ham = hk.readHk(hk_filename,options.version)[0]
print('')

# sanity check of orbitals provided in partial_selec.dat:

for jj in par:
    if jj >= ham.shape[1]:
        raise Exception('one orbital selected in partial_selec.dat is out of range (>'+str(ham.shape[1]-1)+')')





eigVals,eigVecs = np.linalg.eigh(ham)

h_loc = np.zeros((num_par,num_par))
h_loc = np.sum(ham,axis=0)[np.array(par)[:,np.newaxis],np.array(par)]/ham.shape[0]
print(h_loc.shape)
h_loc -= np.eye(num_par)*options.mu
#h_loc *= 0.0
print('local crystal field relative to the fermi energy:')
for ii in range(h_loc.shape[0]):
    for jj in range(h_loc.shape[1]):
        print('{0: 7.5f} {1: 7.5f}j '.format(h_loc[ii,jj].real,h_loc[ii,jj].imag),end='')

    print('')
print('')

crys_filename = os.path.splitext(hk_filename)[0]+'_crystalfield.dat'
print('saving crystal field to '+crys_filename+'\n')
with open(crys_filename,'w') as f:
    f.write('# crsytal field matrix: real(11) imag(11) real(12) imag(12)...\n')
    for ii in range(num_par):
        for jj in range(num_par):
            f.write('{0: 20.16f} {1: 20.16f}j '.format(h_loc[ii,jj].real,h_loc[ii,jj].imag))
        f.write('\n')

if options.matsubara:
    if options.beta is None:
        raise Exception('For matsubara calculation, beta has to be set by -b or --beta')
    z = 1j*green.mats(options.beta, options.nw)
else:
    minV = np.min(eigVals)
    maxV = np.max(eigVals)
    
    if options.wmin is None:
        options.wmin = minV - (0.01)*np.abs(maxV-minV+options.idelta*10)  - options.mu
        print('automatically choosing lower limit of energy axis to '+str(options.wmin) )
    if options.wmax is None:
        options.wmax = maxV + (0.01)*np.abs(maxV-minV+options.idelta*10)  - options.mu
        print('automatically choosing upper limit of energy axis to '+str(options.wmax) )
    
    if options.nw is None:
    
        pass
        # we want an energy resolutio of the broadening
        eRes = options.idelta*2
        options.nw = int((options.wmax - options.wmin) / eRes)
        if options.nw < 2:
            options.nw = 2
        print('automatically choosing number of energy points to '+str(options.nw) )
    
    
    # check energy resolution
    eRes = options.idelta*2
    if options.nw < (options.wmax - options.wmin) / eRes -1:
        print('WARNING:')
        print('The chosen energy resolution is not sufficient to resolve a single')
        print('pole with the current broadening')
    
        print('Choose more energy points (nw > {0:d}): -n {0:d}'.format(int((options.wmax - options.wmin) / eRes)+1))
        broad_good = (options.wmax - options.wmin) / 2.0/options.nw
        print('or increase broadening (idelta > '+str(broad_good)+'): -i '+str(broad_good))
        print('')
    
    z = np.linspace(options.wmin,options.wmax,options.nw ) + 1j*options.idelta

G_loc = green.non_interacting_greens_vecK(z,options.mu,ham,ham.shape[1],
                                        calcDOS=False,
                                        diagonal=False,
                                        eigVecs=eigVecs,
                                        eigVals=eigVals,
                                        cpu_count=options.ncpu)[0]

orb_list=options.orbitals
orb_list = np.array(par)[:,np.newaxis],np.array(par)
hyb = green.hybridization(z,G_loc,par,eps_d=h_loc)
hyb_diag = np.diagonal(hyb,axis1=1,axis2=2)



hyb_filename = os.path.splitext(hk_filename)[0]+'_hyb.dat'
print('saving hybridization to '+hyb_filename)

if options.matsubara:
    style='-o'
    x = z.imag
else:
    style='-'
    x = z.real

z_save = np.zeros((1,z.size))
z_save[0,:] = x

if options.offdiag:
    hyb_save = np.zeros((2*hyb.shape[1]**2,z.size))
    hyb = hyb.reshape(z.size,hyb.shape[1]**2)
    hyb_save[::2,:] = hyb.transpose().real
    hyb_save[1::2,:] = hyb.transpose().imag
else:
    hyb_save = np.zeros((2*hyb_diag.shape[1],z.size))
    hyb_save[::2,:] = hyb_diag.transpose().real
    hyb_save[1::2,:] = hyb_diag.transpose().imag

np.savetxt(hyb_filename,np.vstack((z_save,hyb_save)).transpose()
            ,header='z, real(Delta_1_1), imag(Delta_1_1), real(Delta_2_2), imag(Delta_2_2 ...')


if options.archive:
    import h5py
    h5_filename = os.path.splitext(hk_filename)[0]+'_hyb.h5'
    print('saving everything to '+hyb_filename)
    with h5py.File(h5_filename,'w') as ar:
        ar['z'] = z
        if options.offdiag:
            ar['Delta'] = hyb
        ar['Delta_diag'] = hyb_diag
        ar['crystal_field'] = h_loc

print('plotting...')

f1 = plt.figure(1)
plt.subplot(2,1,1)
plt.plot(x,hyb_diag.real,style)
plt.legend(['orb'+str(x+1) for x in par])
plt.xlabel('w')
plt.ylabel('real(Delta)')

plt.subplot(2,1,2)
plt.plot(x,hyb_diag.imag,style)
plt.xlabel('w')
plt.ylabel('imag(Delta)')

if options.savefigure:
    plot_filename = os.path.splitext(hk_filename)[0]+'_hyb.pdf'
    plt.savefig(plot_filename)
else:
    plt.show()

