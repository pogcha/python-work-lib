#!/usr/bin/env python

import numpy as np
import h5py
from optparse import OptionParser


# input stuff
parser = OptionParser(usage="%prog hk-filename [options]")
parser.add_option("-i", "--iter",
                  help="iteration to grab",
                    default='-1',type="int")
parser.add_option("-p", "--plot",
                  action="store_true",  default=False,
                  help="plot the semilogy curves of accept-pair-tau vs. tau")
       
           
# fix python 2.x behavior to evaluate input. override input with raw_input
(options, args) = parser.parse_args()



if len(args) == 0:
    raise Exception('hdf5 filename not provided! provide by \'set_NCorr.py file.h5\'')
else:
    h5file = args[0]

if options.iter<-1:
    raise Exception('iter (-i, --iter) hsa to be positive. Only Exception is -1 to get last iteration.')

if options.iter == -1:
    print('\ngetting last iteration\n')
else:
    print('\ngetting iteration '+str((options.iter))+'\n')

if options.plot:
    import matplotlib.pyplot as plt

data = []
accins = []
with h5py.File(h5file, "r") as ar:
    if options.iter == -1:
        link = 'dmft-last'
    else:
        link = 'dmft-{0:03d}'.format(options.iter)
    for group in ar[link]:
        if group[:4] == 'ineq':
            link2 = link+'/'+group+'/hist/value'
            data.append(ar[link2][...])
            link3 = link+'/'+group+'/accept-ins/value'
            accins.append(ar[link3][...])
            
print('found '+str(len(data))+' atoms\n')


hist_order = np.arange(1,data[0].shape[2]+1)
globalMaxAtom = []
globalMinZeroAtom = []
globalPercAtom = []
acceptInsAtom = []

print('estimating correlatin time by  NCorr = hist_order / accept-ins')
print('where hist_order is either the index of the maximum')
print('in the expansion order (NCorr_max), the largest order')
print('which is still non-zero (NCorr_zero) or where the order')
print('has fallen to 1% of the maximum (NCorr_0.01)')

print('atom  max_ind  zero_ind  10_ind     accept-ins  NCorr_max  NCorr_zero  NCorr_0.01')
for iA in range(len(data)):

    # get largest index of maximum in the histogram
    listMaxOrbSpin = []
    for iO in range(data[iA].shape[0]):
        for iS in range(data[iA].shape[1]):
            listMaxOrbSpin.append(np.argmax((data[iA][iO,iS,:] )))
            
    globalMaxAtom.append(max(listMaxOrbSpin)+1)
    
    # look for the last expansion order where histogram is not 0
    listMinZeroOrbSpin = []
    for iO in range(data[iA].shape[0]):
        for iS in range(data[iA].shape[1]):
            listMinZeroOrbSpin.append(np.where(data[iA][iO,iS,:] != 0.0)[0][-1])
    
    globalMinZeroAtom.append(max(listMinZeroOrbSpin))
    
    # look for the last expansion order where histogram is fallen to 10% of max
    listPercOrbSpin = []
    for iO in range(data[iA].shape[0]):
        for iS in range(data[iA].shape[1]):
            listPercOrbSpin.append(
                np.argmin(np.abs(data[iA][iO,iS,:] - 0.01*np.max(data[iA][iO,iS,:])))+1)

    globalPercAtom.append(max(listPercOrbSpin))
    
    acceptInsAtom.append(accins[iA][0])
    
    print('  {0:2d}     {1:4d}      {2:4d}    {3:4d}   {4:10.10f}      {5:5d}       {6:5d}     {7:5d}'.
        format(iA+1,globalMaxAtom[iA],globalMinZeroAtom[iA],
               globalPercAtom[iA],acceptInsAtom[iA],
               np.int(np.ceil(globalMaxAtom[iA]/acceptInsAtom[iA])),
               np.int(np.ceil(globalMinZeroAtom[iA]/acceptInsAtom[iA])),
               np.int(np.ceil(globalPercAtom[iA]/acceptInsAtom[iA]))))
    
    if options.plot:
        for iO in range(5):
            for iS in range(2):
                plt.plot(hist_order,data[iA][iO,iS,:],'b')

NCorr_max_criterion = np.array(globalMaxAtom)/np.array(acceptInsAtom)
NCorr_high_criterion = np.array(globalMinZeroAtom)/np.array(acceptInsAtom)
NCorr_perc_criterion = np.array(globalPercAtom)/np.array(acceptInsAtom)


print('')
print(' max     {0:4d}      {1:4d}    {2:4d}   {3:10.10f}      {4:5d}       {5:5d}     {6:5d}'.
        format(max(globalMaxAtom),max(globalMinZeroAtom),
               max(globalPercAtom),max(acceptInsAtom),
               np.int(np.ceil(np.max(NCorr_max_criterion))),
               np.int(np.ceil(np.max(NCorr_high_criterion))),
               np.int(np.ceil(np.max(NCorr_perc_criterion)))))
               
#print('\nlargest NCorr (maximum criterion): {0:d}'.format(np.int(np.ceil(np.max(NCorr_max_criterion)))))
#print('\nlargest case NCorr (maximum criterion): {0:d}'.format(np.int(np.ceil(np.max(NCorr_high_criterion)))))
print('')

if options.plot:
    plt.plot([max(globalMaxAtom),max(globalMaxAtom)],[0,1.1*np.max(data[iA])],'-k',label='position of maximum')
    plt.plot([max(globalMinZeroAtom),max(globalMinZeroAtom)],[0,1.1*np.max(data[iA])],'-r',label='highest non zero')
    plt.plot([max(globalPercAtom),max(globalPercAtom)],[0,1.1*np.max(data[iA])],'-g',label='10% of max')
    plt.xlim(0,max(globalMinZeroAtom)+1)
    plt.xlabel('expansion order')
    plt.ylabel('histogram')
    plt.show()
