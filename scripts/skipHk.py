#!/usr/bin/env python
# -*- coding: utf-8 -*-

# apply lorentioan smearing to VASP (partial) density of states
# the DOS files are assumed to be output from the vdos script

from __future__ import print_function
import python_lib.hk as hk
from optparse import OptionParser


# input stuff
parser = OptionParser(usage="%prog hk-filename [options]")
parser.add_option("-v", "--version",
                  help="header version of H(k)",
                    default='3',type="int")
parser.add_option("-n", "--nskip",
                  help="take only every n'th k-point",
                    default='2',type="int")

# fix python 2.x behavior to evaluate input. override input with raw_input
(options, args) = parser.parse_args()

if len(args) == 0:
    raise Exception('H(k) filename not provided! provide by \'dosHk.py file.dat\'')
else:
    hk_filename = args[0]

print('')
ham, nD, nP, nAt, kVec, nE  = hk.readHk(hk_filename,options.version)
print('')

ham = ham[::options.nskip,...]
kVec = kVec[::options.nskip,...]

hk.writeHk(hk_filename+'_skip',options.version,ham,kVec,nAt,nD,nP,nE)



