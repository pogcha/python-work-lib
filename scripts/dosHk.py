#!/usr/bin/env python
# -*- coding: utf-8 -*-

# apply lorentioan smearing to VASP (partial) density of states
# the DOS files are assumed to be output from the vdos script

from __future__ import print_function
import numpy as np
import python_lib.greens_functions as green
import python_lib.hk as hk
from optparse import OptionParser
import matplotlib.pyplot as plt
import os


# input stuff
parser = OptionParser(usage="%prog hk-filename [options]")
parser.add_option("-v", "--version",
                  help="header version of H(k)",
                    default='3',type="int")
parser.add_option("-m", "--mu",
                  help="chemical potential",
                    default='0.0',type="float")
parser.add_option("-i", "--idelta",
                  help="broadening",
                    default='0.1',type="float")
parser.add_option("-l", "--wmin",
                  help="lower boundary of energy energy-axis",
                    default=None,type="float")
parser.add_option("-u", "--wmax",
                  help="upper boundary of energy energy-axis",
                    default=None,type="float")
parser.add_option("-n", "--nw",
                  help="number of energy points",
                    default=None,type="int")
parser.add_option("-c", "--ncpu",
                  help="number of cpus to use",
                    default=None,type='int')
parser.add_option("-s", "--savefigure",action="store_true",
                  help="save the plot instead of showing it",
                    default=False)
# fix python 2.x behavior to evaluate input. override input with raw_input
(options, args) = parser.parse_args()

if len(args) == 0:
    raise Exception('H(k) filename not provided! provide by \'dosHk.py file.dat\'')
else:
    hk_filename = args[0]

# reading partial_selec.dat
num_par = 0
if os.path.isfile('partial_selec.dat') :
    with open('partial_selec.dat') as f:
        par = []
        lines = f.readlines()
        for ll in lines:
            if ll[0] == '#':
                pass
            else:
                par.append([int(x) for x in ll.split()])

    num_par = len(par)
    print('\nfound file partial_selec.dat')
    print('plotting partial dos summed over these orbitals:')
    for ii in range(num_par):
        print('   '+str(ii)+': '+str(par[ii]))

else:
    print('\ndid not find file partial_selec.dat: not summing over certain orbitals.')
    print('Example for file partial_selec.dat assuming 9 VASP ordered orbitals')
    print('which would plot s-, p-, t2g-, and eg-orbitals')
    print('\n0')
    print('1 2 3')
    print('4 5 7')
    print('6 8\n')

print('')
ham = hk.readHk(hk_filename,options.version)[0]
print('')

# sanity check of orbitals provided in partial_selec.dat:
for ii in range(num_par):
    for jj in par[ii]:
        if jj >= ham.shape[1]:
            raise Exception('one orbital selected in partial_selec.dat is out of range (>'+str(ham.shape[1]-1)+')')


eigVals,eigVecs = np.linalg.eigh(ham)

minV = np.min(eigVals)
maxV = np.max(eigVals)




if options.wmin is None:
    options.wmin = minV - (0.01)*np.abs(maxV-minV+options.idelta*10)  - options.mu
    print('automatically choosing lower limit of energy axis to '+str(options.wmin) )
if options.wmax is None:
    options.wmax = maxV + (0.01)*np.abs(maxV-minV+options.idelta*10)  - options.mu
    print('automatically choosing upper limit of energy axis to '+str(options.wmax) )

if options.nw is None:

    pass
    # we want an energy resolutio of the broadening
    eRes = options.idelta*2
    options.nw = int((options.wmax - options.wmin) / eRes)
    if options.nw < 2:
        options.nw = 2
    print('automatically choosing number of energy points to '+str(options.nw) )


# check energy resolution
eRes = options.idelta*2
if options.nw < (options.wmax - options.wmin) / eRes -1:
    print('WARNING:')
    print('The chosen energy resolution is not sufficient to resolve a single')
    print('pole with the current broadening')

    print('Choose more energy points (nw > {0:d}): -n {0:d}'.format(int((options.wmax - options.wmin) / eRes)+1))
    broad_good = (options.wmax - options.wmin) / 2.0/options.nw
    print('or increase broadening (idelta > '+str(broad_good)+'): -i '+str(broad_good))
    print('')

z = np.linspace(options.wmin,options.wmax,options.nw ) + 1j*options.idelta

dos = green.non_interacting_greens_vecK(z,options.mu,ham,ham.shape[1],True,eigVecs,eigVals,cpu_count=options.ncpu)[0]

dost = np.sum(dos,axis=1)


n_el_dos = 2*np.sum(dos[z.real<0,:])*(z[1]-z[0]).real
print('total electrons:',n_el_dos)

if num_par != 0:
    dosp = np.zeros((z.size,num_par))
    n_el_all_orbs = 2*np.sum(dos[z.real<0,:],axis=0)*(z[1]-z[0]).real
    for ii in range(num_par):
        dosp[:,ii] = np.sum(dos[:,par[ii]],axis=1)
        n_el_orb = np.sum(n_el_all_orbs[par[ii]])
        print('number of electrons in orbital set', ii,',', str(par[ii])+':', n_el_orb)

print()
dos_filename = os.path.splitext(hk_filename)[0]+'_dos.dat'
print('saving total and partial dos to '+dos_filename)

z_save = np.zeros((1,z.size))
dos_save = np.zeros((dos.shape[1]+1,z.size))
z_save[0,:] = z.real
dos_save[0,:] = dost
dos_save[1:,:] = dos.transpose()

np.savetxt(dos_filename,np.vstack((z_save,dos_save)).transpose()
            ,header='z, total dos,  dos orb 1, dos orb 2, ...')

print('plotting...')
plt.plot(z.real,dost,'-k',label='total')
for ii in range(num_par):
    plt.plot(z.real,dosp[:,ii],label=str(par[ii]))
plt.legend()
plt.xlabel('w')
plt.ylabel('DOS')

if options.savefigure:
    plot_filename = os.path.splitext(hk_filename)[0]+'_dos.pdf'
    plt.savefig(plot_filename)
else:
    plt.show()
