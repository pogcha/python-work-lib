from __future__ import print_function
import numpy as np


def periodize(index_x,index_y,N):
    return (index_x)%N+N*((index_y)%N)    

def square_lattice_real_space(tt,LL,ttp=0.0,boundary='periodic'):
    # nearest neighbor tight-binding lattice
    assert boundary == 'periodic'
    
    numSites = LL**2    
    
    ham = np.zeros((numSites,numSites))
    
    for ix in range(LL):
        for iy in range(LL):
            #            
            # Hoppings
            #

            # to the right
            ham[periodize(ix,iy,LL),periodize(ix+1,iy,LL)] += tt
            # to the left
            ham[periodize(ix,iy,LL),periodize(ix-1,iy,LL)] += tt
            # to the up
            ham[periodize(ix,iy,LL),periodize(ix,iy+1,LL)] += tt
            # to the down
            ham[periodize(ix,iy,LL),periodize(ix,iy-1,LL)] += tt
            
            #
            # t prime
            #
            
            ham[periodize(ix,iy,LL),periodize(ix+1,iy+1,LL)] += ttp
            ham[periodize(ix,iy,LL),periodize(ix-1,iy+1,LL)] += ttp
            ham[periodize(ix,iy,LL),periodize(ix+1,iy-1,LL)] += ttp
            ham[periodize(ix,iy,LL),periodize(ix-1,iy-1,LL)] += ttp
            
    ham = np.expand_dims(ham,axis=-1)
    ham = np.expand_dims(ham,axis=-1)
    return ham
