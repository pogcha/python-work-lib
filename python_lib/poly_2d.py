import numpy as np
from scipy.optimize import minimize


def cost_test(coeffs,x,y,z,zerr,poly_x,poly_y):
    coeffs = np.reshape(coeffs,(poly_x+1,poly_y+1))
    fun = evaluate_poly_2d(x,y,coeffs)
    return np.sum((fun - z.transpose())**2/np.sqrt(zerr.transpose()))

def cost(coeffs,x,y,D,Derr,G,Gerr,poly_x,poly_y):
    coeffD = np.reshape(coeffs[:coeffs.size//2],(poly_x+1,poly_y+1))
    coeffG = np.reshape(coeffs[coeffs.size//2:],(poly_x+1,poly_y+1))
    funD = evaluate_poly_2d(x,y,coeffD)
    funG = evaluate_poly_2d(x,y,coeffG)
    deviationD = np.sum(((funD - D.transpose())/D.transpose())**2/np.sqrt(Derr.transpose()))
    deviationG = np.sum(((funG - G.transpose())/G.transpose())**2/np.sqrt(Gerr.transpose()))
    return deviationD + deviationG


def evalDeriv_at_point(x,y,coeffs,poly_x,poly_y):
    
    coeffD = np.reshape(coeffs[:coeffs.size//2],(poly_x+1,poly_y+1))
    coeffG = np.reshape(coeffs[coeffs.size//2:],(poly_x+1,poly_y+1))
    
    pFitGradD_U, pFitGradD_t = grad_poly_2d(coeffD)
    pFitGradG_U, pFitGradG_t = grad_poly_2d(coeffG)
    
    derivs = np.zeros((2,2))
    derivs[0,0] = evaluate_poly_2d(x,y,pFitGradD_U)
    derivs[0,1] = evaluate_poly_2d(x,y,pFitGradD_t)
    derivs[1,0] = evaluate_poly_2d(x,y,pFitGradG_U)
    derivs[1,1] = evaluate_poly_2d(x,y,pFitGradG_t)
    
    return derivs
    
    
def polyfit_2d_free_energy(x,y,xP,yP,D,Derr,G,Gerr,poly_x,poly_y,startD,startG):
    # 

    coeffD0 = np.reshape(startD,startD.size,order='C')
    coeffG0 = np.reshape(startG,startG.size,order='C')
    coeff0 = np.concatenate((coeffD0,coeffG0))

    sym = lambda x: evalDeriv_at_point(xP,yP,x,poly_x,poly_y)[0,1] - evalDeriv_at_point(xP,yP,x,poly_x,poly_y)[1,0] - 0.0    
    conv_D_U = lambda x: -evalDeriv_at_point(xP,yP,x,poly_x,poly_y)[0,0] 
    conv_G_t = lambda x: -evalDeriv_at_point(xP,yP,x,poly_x,poly_y)[1,1] 
    cons = [{'type' : 'eq', 'fun' : sym},
               {'type' : 'ineq', 'fun' : conv_D_U},
               {'type' : 'ineq', 'fun' : conv_G_t}]    
    
    out = minimize(cost,
                   x0 = coeff0,
                   args = (x,y,D,Derr,G,Gerr,poly_x,poly_y),
                   )#constraints=cons)
    print(out)
    coeff = out.x
    coeffD = np.reshape(coeff[:coeff.size//2],(poly_x+1,poly_y+1))
    coeffG = np.reshape(coeff[coeff.size//2:],(poly_x+1,poly_y+1))
    return coeffD, coeffG
    
    
def polyfit_2d_free_energy_test(x,y,D,Derr,G,Gerr,poly_x,poly_y,startD,startG):
    # this is a slow replica of least-square fit together with function cost_test    
    coeff0 = np.reshape(startD,startD.size,order='C')
    out = minimize(cost_test,
                   x0 = coeff0,
                   args = (x,y,D,Derr,poly_x,poly_y))
    coeff = out.x
    coeff = np.reshape(coeff,(poly_x+1,poly_y+1))
    return coeff
    

def polyfit_2d(x,y,z,zerr,poly_x,poly_y):
    # perform 2d fit with polynomial
    # returns a (poly_x times poly_y) matrix of coefficient defined as follows
    # coeffs[0,0] + coeffs[1,0]*x + coeffs[2,0]*x**2
    #             + coeffs[1,1]*x*y + coeffs[2,1]*y*x**2
    #    ...
    assert x.size == z.shape[0]
    assert y.size == z.shape[1]
    assert z.shape == zerr.shape
    
    X, Y = np.meshgrid(x, y, copy=False,indexing='ij')
    X = X.flatten()
    Y = Y.flatten()
    # coeff matrix
    vander = []
    for ix in range(poly_x+1):
        for iy in range(poly_y+1):
            vander.append(X**ix*Y**iy)
    A = np.array(vander).T
    B = z.flatten()
    
    if zerr is None:
        Aw = A
        Bw = B
    else:
        Berr = zerr.flatten()
        W = 1.0/Berr
        Aw = A * np.sqrt(W[:,np.newaxis])
        Bw = B * np.sqrt(W)
    coeff, r, rank, s = np.linalg.lstsq(Aw, Bw, rcond=None)
    coeff = np.reshape(coeff,(poly_x+1,poly_y+1))
    return coeff
    
def evaluate_poly_2d(x,y,coeffs):
    
    X, Y = np.meshgrid(x, y, copy=False)
    z = np.zeros(X.shape)
    for ix in range(coeffs.shape[0]):
        for iy in range(coeffs.shape[1]):
            z += coeffs[ix,iy] * (X**ix*Y**iy)
    return z
    
def grad_poly_2d(coeffs):
    
    coeffsGrad_x = coeffs[1:,:] * np.arange(1,coeffs.shape[0])[:,np.newaxis]
    coeffsGrad_y = coeffs[:,1:] * np.arange(1,coeffs.shape[1])[np.newaxis,:]

    return coeffsGrad_x,coeffsGrad_y
