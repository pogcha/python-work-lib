from __future__ import print_function
import numpy as np
import time
import multiprocessing
from functools import partial
import python_lib.linalg as linalg
import os
from scipy import optimize as opt

def fit_real(x,c1,c2):
    return -c1/x**2 + c2/x**4

def fit_imag(x,c1,c2):
    return -c1/x + c2/x**3

def moment_fitter(iw,giw,w_cutoff):
    ''' calculates the first 4 moments of a matsubara green function by
        fitting -m1/iw + m2/iw^2 - m3/iw^3 + m4/iw^4 in the tail region
    
    Parameters
    --------
    iw : array
        array of energies 1j w_n
    gf: complex array
        matsubara green function
    w_cutoff : ndarray
        index of cut_off frequency (frequqncies larger than this index
        are included in the fit)

        
    Returns
    --------
    moments: array
        the 4 moments of the green function
    moments_err: array
        the errors of the  4 moments
    '''
    
    if iw[0].imag != 0:
        iw = iw.imag
        
    if iw[0] < 0:
        giw = giw[giw.size//2:]
        iw = iw[iw.size//2:]
    
    out_real = opt.curve_fit(fit_real,iw[w_cutoff:],giw.real[w_cutoff:])
    
    out_imag = opt.curve_fit(fit_imag,iw[w_cutoff:],giw.imag[w_cutoff:])
        
    moments = np.array([out_imag[0][0],out_real[0][0],out_imag[0][1],out_real[0][1]])
    moments_err = np.array([out_imag[1][0,0],out_real[1][0,0],out_imag[1][1,1],out_real[1][1,1]])
    
    return moments,moments_err

def fermiFunc(beta,w,mu=0.0,T0=False):
    #%% 
    if T0:
        fermiFunction =  (-np.sign(w - mu)+1)*0.5
    else:
        # this leads to overflows for small betas
        #fermiFunction = 1.0 / ( np.exp((energies-par.mu)*par.beta) + 1 ) 
        # this does not lead to overflow:
        fermiFunction = 0.5* (1.0 - np.tanh((w-mu)*beta*0.5))
    return fermiFunction
    
def nonInteracting_densMat(beta,eigVals,eigVecs,mu,T0=False):
    diag_densMat = fermiFunc(beta,eigVals,mu,T0)
    densMat = linalg.fromDiagToOrig_diagonalStart(eigVecs,diag_densMat)
    return densMat
def fermiEnergy(eigVals,numElecs):
    eigValsSort = np.sort(eigVals.flatten())
    nK = eigVals.shape[0]
    eF = eigValsSort[numElecs*nK//2]
    return eF
    
def init_green(Hk, z, numOrbsg, calcDOS, diagonal,nodiag=True,kSum=True):
    
    numK = Hk.shape[0]
    numW = z.size
    numOrbs = Hk.shape[-1]
    
    # since parallization is done w.r.t. the energy axis, for a single 
    # energy point we want to benefit from MKL threading (assuming numpy is linked to MKL...)
    # if we have multiple energy points, MKL threading should always be slower
    # than naive parallelization over energy
    os.environ['OMP_NUM_THREADS'] = '1'
    
    if (calcDOS == True) and (kSum == False):
        raise(Exception('for calcDOS=True, kSum=True is required'))
    if z.size == 1:
        print('   single point z = '+str(z[0]))
        os.environ['OMP_NUM_THREADS'] = str(multiprocessing.cpu_count())
    elif z[0].imag == z[-1].imag:
        # assuming real axis calculation with broadening:
        print('   real axis from '+str(z[0].real)+' to '+str(z[-1].real)
                +' in '+str(z.size)+' steps with i = '+str(z[0].imag))
    elif z[0].real == 0.0 and z[-1].real == 0.0:
        # assumin matsubara frequencies
        print('   '+str(z.size)+' matsubara frequencies with beta = '
                    +str(2.0/(z[1]-z[0]).imag*np.pi))
        os.environ['OMP_NUM_THREADS'] = '1'
    else:
        print('   did not recognize type of z mesh')
        
    if len(Hk.shape) == 4:
        spin = True
    else:
        spin = False
        
    if calcDOS:
        if spin:
            if kSum:
                out = np.zeros((numW,numOrbsg,2),dtype=float)
            else:
                out = np.zeros((numW,numK,numOrbsg,2),dtype=float)
        else:
            if kSum:
                out = np.zeros((numW,numOrbsg),dtype=float)
            else:
                out = np.zeros((numW,numK,numOrbsg),dtype=float)
    else:
        if diagonal:
            if spin:
                if kSum:
                    out = np.zeros((numW,2,numOrbsg),dtype=complex)
                else:
                    out = np.zeros((numW,numK,2,numOrbsg),dtype=complex)
            else:
                if kSum:
                    out = np.zeros((numW,numOrbsg),dtype=complex)
                else:
                    out = np.zeros((numW,numK,numOrbsg),dtype=complex)
        else:
            if spin:
                if kSum:
                    out = np.zeros((numW,2,numOrbsg,numOrbsg),dtype=complex)
                else:
                    out = np.zeros((numW,numK,2,numOrbsg,numOrbsg),dtype=complex)
            else:
                if kSum:
                    out = np.zeros((numW,numOrbsg,numOrbsg),dtype=complex)
                else:
                    out = np.zeros((numW,numK,numOrbsg,numOrbsg),dtype=complex)
    if nodiag == False:
        print( '   diagonalizing on all k-points...',end=' ')
        eigVals,eigVecs = np.linalg.eigh(Hk[:,:,:])
        print( 'done')
    else:
        eigVals = None
        eigVecs = None
    
    return out, numK, numW, numOrbs, eigVals, eigVecs
    
def normGreen(out,calcDOS,numK,kSum):
    if calcDOS:
        out *= -1.0/np.pi
    if kSum:
        out *= 1.0/float(numK)
    return out

def non_interacting_greens(z,mu,Hk,numOrbsg=None,calcDOS=False,
                           diagonal=True,eigVecs=None,eigVals=None,kSum=True):
    ''' calculates the noninteracting greens function G for some given H(k) and mu on energies z.  
    
    Parameters
    --------
    z : array
        array of energies (w + 1j) or 1j w_n
    mu : float
        chemical potential
    Hk : ndarray
        Hamiltonian of shape (nk,nOrb,nOrb)
    numOrbsg : int
        dimension of a (upper left) block of the hamiltonian for which
        the green funktion is calculated. Default None (numOrbsg = nOrb)
    calcDOS : logical
        toggles if the DOS (neg. imag. part of G) is calculated
        or the full greensfunction
    diagonal : logical
        toggles of only the diagonal of G is calculated
    eigVecs : ndarray
        Eigenvectors of Hk. shape (nk, nOrb, nOrb)
    eigVals : ndarray
        Eigenvalues of Hk. shape (nk, nOrb). If both eigVals and eigVecs
        are present, they are not calculated from H(k). Better be sure
        they actually belong to Hk!
    kSum : logical
        toggles if the k-summed greensfunction is calculated or not
        
    Returns
    --------
    out : ndarray
        Greensfunction. If kSum == False of shape 
        (nz,nOrb) if calcDOS == True or diagonal == True
        (nz,nOrb,nOrb) if calcDOS = False and diagonal == False.
        If kSum == True of shape 
        (nz,nk,nOrb) if calcDOS == True or diagonal == True
        (nz,nk,nOrb,nOrb) if calcDOS = False and diagonal == False
    eigVecs : ndarray
        eigenvectors of H(k). shape (nk,nOrb,nOrb)
    eigVals : ndarray
        eigenvectors of H(k). shape (nk,nOrb)
    
    '''
    
    #
    # for numOrbsg < dim(Hk), only a block greenfunction of dimension numOrbsg is calculated
    # this block is assumed to be the upper left part of Hk
    #
    # First H(k) is diagonalized, then the diagonal greensfunction is trivialy inverted.
    # Then the greenfunction is transformed back to the old basis states using the eigVecs
    #
    # Eigenvalues and Eigenvectors can be supplied if they were calculated before.
    #
    # diagonal toggles if only the diagonal of the greensfunction is calculated
    #
    # calcDOS toggles if the DOS is calculated or the the full greensfunction
    # , i.e., the neg. imaginary part of the greensfunction is taken.

    print( 'calculating non interacting greensfunction...')
     
    start = time.time()
    
    

    print('   inverting...',end=' ')

    if numOrbsg is None:
        numOrbsg = Hk.shape[1]

    if numOrbsg == Hk.shape[1]:
        out, numK, numW, numOrbs, _, _ = init_green(Hk, z, numOrbsg, calcDOS, diagonal,nodiag=True,kSum=kSum)
        for iK in range(numK):
            for iw in range(numW):
                an1 = np.eye(numOrbs)*(z[iw]+mu)  - Hk[iK,:,:]
                if calcDOS:
                    out[iw,:] += np.imag(np.diagonal(np.linalg.inv(an1)))
                else:
                    if diagonal:
                        if kSum:
                            out[iw,:] += np.diagonal(np.linalg.inv(an1))
                        else:
                            out[iw,iK,:] = np.diagonal(np.linalg.inv(an1))
                    else:
                        if kSum:
                            out[iw,:,:] += (np.linalg.inv(an1))
                        else:
                            out[iw,iK,:,:] = (np.linalg.inv(an1))
    else:
        if (eigVals is None) or (eigVecs is None):
            out, numK, numW, numOrbs, eigVals, eigVecs = init_green(Hk, z, numOrbsg, calcDOS, diagonal,nodiag=False,kSum=kSum)
        else:
            out, numK, numW, numOrbs, _, _ = init_green(Hk, z, numOrbsg, calcDOS, diagonal,nodiag=True,kSum=kSum)
            print('   using supplied eigen vectors and values.')
            print('   better be sure they correspond to the H(k)')
        for iK in range(numK):
            eV = eigVecs[iK,:numOrbsg,:numOrbs]
            eVT = eV.transpose().conjugate()
            for iw in range(numW):
                an = 1/(z[iw]+mu-eigVals[iK,:numOrbs])
                if calcDOS:
                    if kSum:
                        out[iw,:] += np.imag(np.einsum('ij,ji->i',(an*eV),eVT))
                    else:
                        out[iw,iK,:] = np.imag(np.einsum('ij,ji->i',(an*eV),eVT))
                else:
                    if diagonal:
                        if kSum:
                            out[iw,:] += (np.einsum('ij,ji->i',(an*eV),eVT))
                        else:
                            out[iw,iK,:] = (np.einsum('ij,ji->i',(an*eV),eVT))
                    else:
                        if kSum:
                            out[iw,:,:] += (np.einsum('ij,jk->ik',(an*eV),eVT))
                        else:
                            out[iw,iK,:,:] = (np.einsum('ij,jk->ik',(an*eV),eVT))
                        
                    
    out = normGreen(out,calcDOS,numK,kSum=kSum)

    print('done')
    print('took', time.time()-start,'s')
    return out, eigVecs, eigVals

def singleW_Full(ziw,mu,Hk,numOrbs,calcDOS,diagonal,kSum): 
    anK = (np.eye(numOrbs)*(ziw+mu))[np.newaxis,:,:] - Hk[:,:,:]
    # invert on all k-points:
    invK = np.linalg.inv(anK)
    if calcDOS:
        out_iw = np.diagonal(np.imag(invK),axis1=1,axis2=2)
    else:
        if diagonal:
            out_iw = np.diagonal((invK),axis1=1,axis2=2)
        else:
            out_iw = invK
    if kSum:
        out_iw = np.sum(out_iw,axis=0)
    return out_iw 

def non_interacting_greens_vecK(z,mu,Hk,numOrbsg=None,calcDOS=False,diagonal=True,
                                eigVecs=None,eigVals=None,cpu_count=1,kSum=True):
    ''' vectorized calculation of noninteracting greens function G for some given H(k) and mu on energies z.  
    
    Parameters
    --------
    z : array
        array of energies (w + 1j) or 1j w_n
    mu : float
        chemical potential
    Hk : ndarray
        Hamiltonian of shape (nk,nOrb,nOrb)
    numOrbsg : int
        dimension of a (upper left) block of the hamiltonian for which
        the green funktion is calculated. Default None (numOrbsg = nOrb)
    calcDOS : logical
        toggles if the DOS (neg. imag. part of G) is calculated
        or the full greensfunction
    diagonal : logical
        toggles of only the diagonal of G is calculated
    eigVecs : ndarray
        Eigenvectors of Hk. shape (nk, nOrb, nOrb)
    eigVals : ndarray
        Eigenvalues of Hk. shape (nk, nOrb). If both eigVals and eigVecs
        are present, they are not calculated from H(k). Better be sure
        they actually belong to Hk!
    cpu_count : int
        number of cores to do calculations on
    kSum : logical
        toggles if the k-summed greensfunction is calculated or not
        
    Returns
    --------
    out : ndarray
        Greensfunction. If kSum == False of shape 
        (nz,nOrb) if calcDOS == True or diagonal == True
        (nz,nOrb,nOrb) if calcDOS = False and diagonal == False.
        If kSum == True of shape 
        (nz,nk,nOrb) if calcDOS == True or diagonal == True
        (nz,nk,nOrb,nOrb) if calcDOS = False and diagonal == False
    eigVecs : ndarray
        eigenvectors of H(k). shape (nk,nOrb,nOrb)
    eigVals : ndarray
        eigenvectors of H(k). shape (nk,nOrb)
    
    '''
    if cpu_count is None:
        cpu_count = multiprocessing.cpu_count()

    pool = multiprocessing.Pool(cpu_count)
    
    # does the same as non_interacting_greens() but does so vectorized for the k-points. This is of course
    # more memory hungry but is about a factor of 50 faster than the non-vectorized version

    start = time.time()
    print( 'calculating non interacting greensfunction...') 
    
    if numOrbsg is None:
        numOrbsg = Hk.shape[1]

    if numOrbsg == Hk.shape[1] and calcDOS == False:
        out, numK, numW, numOrbs, _, _ = init_green(Hk, z, numOrbsg, calcDOS, diagonal,nodiag=True,kSum=kSum)
        print('   inverting...',end=' ')
        #Parallel(n_jobs=2)(delayed(singleW_Full)(z_s) for z_s in z )
        
        out = pool.map(partial(singleW_Full,mu=mu,Hk=Hk,numOrbs=numOrbs,calcDOS=calcDOS,diagonal=diagonal,kSum=kSum),z)
        out = np.array(out)
                
    else:
        if (eigVals is None) or (eigVecs is None):
            out, numK, numW, numOrbs, eigVals, eigVecs = init_green(Hk, z, numOrbsg, calcDOS, diagonal,nodiag=False,kSum=kSum)
        else:
            out, numK, numW, numOrbs, _, _ = init_green(Hk, z, numOrbsg, calcDOS, diagonal,nodiag=True,kSum=kSum)
            print('   using supplied eigen vectors and values.')
            print('   better be sure they correspond to the H(k)')
        print('   inverting...',end=' ')
        for iw in range(numW):
            eV = eigVecs[:,:numOrbsg,:numOrbs]
            eVT = np.transpose(eV,axes=(0,2,1)).conjugate()
            an = 1/((z[iw]+mu)-eigVals[:,:numOrbs])
            if calcDOS:
                allk = np.imag(np.einsum('kij,kji->ki',(an[:,np.newaxis,:]*eV),eVT))
            else:
                if diagonal:
                    allk = np.einsum('kij,kji->ki',(an[:,np.newaxis,:]*eV),eVT)
                else:
                    allk = np.einsum('kij,kjl->kil',(an[:,np.newaxis,:]*eV),eVT)
            if kSum:
                out[iw,...] = np.sum(allk,axis=0)
            else:
                out[iw,...] = allk
                
    out = normGreen(out,calcDOS,numK,kSum=kSum)
    
    print('done')
    print('took', time.time()-start,'s')
    return out, eigVecs, eigVals
    
    
def hybridization(w, greensfunction, orb_list, eps_d=None):

    # calculates the hybridization function Delta from a greensfunction via
    # D = w - G^-1
    
    numOrbsg = len(orb_list)
    
    numW = w.size
    
    if eps_d is None:
        eps_d = np.zeros((numOrbsg,numOrbsg))
    
    eps_d = np.array(eps_d)    
    
    if eps_d.size == 1:
        eps_d = eps_d*np.ones((1,1))
        
    oneW = np.zeros(shape=(numW,numOrbsg,numOrbsg))
    for iw in range(numW):
        oneW[iw,:,:] = np.eye(numOrbsg)
        
    Delta = w[:,np.newaxis,np.newaxis]*oneW - eps_d[np.newaxis,:,:] - np.linalg.inv(greensfunction[:,np.array(orb_list)[:,np.newaxis],np.array(orb_list)])
    
    return Delta
    
def interacting_greens(z, mu, Hk, sigmaMat, calcDOS=False, diagonal=True,kSum=True):
    ''' calculation of interacting greens function G for some given H(k), Sigma, and mu on energies z.  
    
    Parameters
    --------
    z : array
        array of energies (w + 1j) or 1j w_n. shape (nz)
    mu : float
        chemical potential
    Hk : ndarray
        Hamiltonian of shape (nk,nOrb,nOrb)
    sigmaMat : ndarray
        self energy of shape (nz, nOrb, nOrb)
    calcDOS : logical
        toggles if the DOS (neg. imag. part of G) is calculated
        or the full greensfunction
    diagonal : logical
        toggles of only the diagonal of G is calculated
    kSum : logical
        toggles if the k-summed greensfunction is calculated or not
        
    Returns
    --------
    out : ndarray
        Greensfunction. If kSum == False of shape 
        (nz,nOrb) if calcDOS == True or diagonal == True
        (nz,nOrb,nOrb) if calcDOS = False and diagonal == False.
        If kSum == True of shape 
        (nz,nk,nOrb) if calcDOS == True or diagonal == True
        (nz,nk,nOrb,nOrb) if calcDOS = False and diagonal == False
    
    '''

    assert Hk.shape[1:] == sigmaMat.shape[1:]

    start = time.time()
    print( 'calculating interacting greensfunction...')  
    
    if len(Hk.shape) == 4:
        spin = True
    else:
        spin = False
        
    out, numK, numW, numOrbs, eigVals, eigVecs = init_green(Hk, z, Hk.shape[-1], calcDOS, diagonal,nodiag=True,kSum=kSum)
    
    for iK in range(numK):
        for iw in range(numW):
            z_mat = np.eye(numOrbs)*(z[iw]+mu)
            if spin == True:
                z_mat = np.stack((z_mat,z_mat),axis=0)
            an1 = z_mat - sigmaMat[iw,...] - Hk[iK,...]
            if calcDOS:
                if kSum:
                    out[iw,...] += np.imag(np.diagonal(np.linalg.inv(an1)))
                else:
                    out[iw,iK,...] = np.imag(np.diagonal(np.linalg.inv(an1)))
            else:
                if diagonal:
                    if kSum:
                        out[iw,...] += np.diagonal(np.linalg.inv(an1))
                    else:
                        out[iw,iK,...] = np.diagonal(np.linalg.inv(an1))
                else:
                    if kSum:
                        out[iw,...] += (np.linalg.inv(an1))
                    else:
                        out[iw,iK,...] = (np.linalg.inv(an1))
                
    out = normGreen(out,calcDOS,numK,kSum=kSum)
    
    print('took', time.time()-start,'s')
    return out
 
def singleW_Full_Sigma(en_sig_tuple,mu,Hk,numOrbs,calcDOS,diagonal,kSum): 
    ziw = en_sig_tuple[0]
    sigmaMatiw = en_sig_tuple[1]
    anK = (np.eye(numOrbs)*(ziw+mu) - sigmaMatiw)[np.newaxis,:,:] - Hk[:,:,:]
    # invert on all k-points:
    invK = np.linalg.inv(anK)
    if calcDOS:
        out_iw = np.diagonal(np.imag(invK),axis1=1,axis2=2)
    else:
        if diagonal:
            out_iw = np.diagonal((invK),axis1=1,axis2=2)
        else:
            out_iw = invK
    if kSum:
        out_iw = np.sum(out_iw,axis=0)

    return out_iw 

def interacting_greens_vecK(z, mu, Hk, sigmaMat, calcDOS=False, diagonal=True,
                            cpu_count=1,kSum=True):
    ''' vectorized calculation of interacting greens function G for some given H(k), Sigma, and mu on energies z.  
    
    Parameters
    --------
    z : array
        array of energies (w + 1j) or 1j w_n. shape (nz)
    mu : float
        chemical potential
    Hk : ndarray
        Hamiltonian of shape (nk,nOrb,nOrb) or (nk,2,nOrb,nOrb) for spin
    sigmaMat : ndarray
        self energy of shape (nz, nOrb, nOrb) or (nz,2,nOrb,nOrb) for spin
    calcDOS : logical
        toggles if the DOS (neg. imag. part of G) is calculated
        or the full greensfunction
    diagonal : logical
        toggles of only the diagonal of G is calculated
    cpu_count : int
        number of cores to do calculation on
    kSum : logical
        toggles if the k-summed greensfunction is calculated or not
        
    Returns
    --------
    out : ndarray
        Greensfunction. If kSum == False of shape 
        (nz,nOrb) if calcDOS == True or diagonal == True
        (nz,nOrb,nOrb) if calcDOS = False and diagonal == False.
        If kSum == True of shape 
        (nz,nk,nOrb) if calcDOS == True or diagonal == True
        (nz,nk,nOrb,nOrb) if calcDOS = False and diagonal == False.
        For spinfull Hk and sigma acquire additional dimension of size 2
        before orbital indices
    
    '''
    # Does the same as interacting_greens() but vectorized for all k-points. This is more memory hungry
    # but a lot faster.

    

    if cpu_count is None:
        cpu_count = multiprocessing.cpu_count()

    if cpu_count == 1:
        pass
    else:
        pool = multiprocessing.Pool(cpu_count)    
    
    start = time.time()
    print( 'calculating interacting greensfunction...')  
    
    assert z.size == sigmaMat.shape[0]
    assert Hk.shape[1:] == sigmaMat.shape[1:]

    out, numK, numW, numOrbs, eigVals, eigVecs = init_green(Hk, z, Hk.shape[-1], calcDOS, diagonal,nodiag=True,kSum=kSum)
    # make list of (z, sigma) tuple
    if cpu_count > 1:
        z_sig_list = []
        for iw in range(numW):
            z_sig_list.append((z[iw],sigmaMat[iw,...]))
        out = pool.map(partial(singleW_Full_Sigma,mu=mu,Hk=Hk,numOrbs=numOrbs,calcDOS=calcDOS,diagonal=diagonal,kSum=kSum),z_sig_list)
        out = np.array(out)
    else:
        for iw in range(numW):
            out_iw = singleW_Full_Sigma((z[iw],sigmaMat[iw,...]),mu,Hk,numOrbs,calcDOS,diagonal,kSum)
            if kSum:
                out[iw,...] = out_iw
            else:
                out[iw,...] = out_iw
    
    out = normGreen(out,calcDOS,numK,kSum=kSum)
    
    print('took', time.time()-start,'s')
    return out

def mats(beta,nMats):
    return np.arange(1,nMats*2,2)*np.pi/beta
    
def fold_mats(ff,iw):
    # this reshapes a function ff on positive and negative N mats frequencies from
    # shape [N,...] to [2,N/2,...] where ff[0,i,...] and ff[1,i,...] are 
    # ff(iw) and ff[-iw]. This makes a symmetric sumation very easy.
    if ff.shape[0] != iw.size:
        raise Exception('first index of function to fold must correspond to mats freq!')
    return np.array((ff[iw.size//2:,...],ff[:iw.size//2,...][::-1]))
    
def matsubara_sum_cum(gg,beta,iw,fit_last,poly,A):

    # do some checks:
    assert gg.shape[0] == iw.size
    if ((iw[0] < 0) and (iw[-1] > 0)) == False:
        raise Exception('matsubara_sum_cum only implemented for neg. and pos. freq.')
        
    #prepare the data to do cumulative symetric sum
    # subtract the analytic 1j/w with a prefactor A

    # add dimensions to iw according to the dimension of gg
    iw_f = iw.copy()    
    for ii in range(len(gg.shape)-1):
        iw_f = np.expand_dims(iw_f,axis=-1)

    gg_fold =fold_mats(gg[:,...]-A*1j/iw_f,iw)
    iw_fold = fold_mats(iw,iw)
    # do the sum (this is probably highly ineffective)
    
    # naive version: do the cumulative sum for all frequencies:
    #cum = np.cumsum(np.sum(gg_fold,axis=0),axis=0)
    
    # faster version: do summation up to Niw-fit_last
    # then do cummulative sum from there on
    sumFirst = np.sum(np.sum(gg_fold,axis=0)[:iw.size//2-fit_last,...],axis=0)
    addCum = np.cumsum(np.sum(gg_fold,axis=0)[-(fit_last):,...],axis=0)
    cum = sumFirst[np.newaxis,...] + addCum
    
    # fit the result vs. 1/iw with polynomial
    out = np.polyfit(1/iw_fold[0,-fit_last:],cum[-fit_last:,:,0],poly)
  
    # do not forget the 1/beta and A*0.5 from the analytic term
    return out[-1,:]/beta+A*0.5
