import numpy as np

def fromDiagToOrig_diagonalStart(eigVecs,matrix_to_transform):
    
    if len(eigVecs.shape) == 2:
        eigVecs = eigVecs[np.newaxis,:,:]
        matrix_to_transform = matrix_to_transform[np.newaxis,...]
        single = True
    else:
        single = False
    if len(matrix_to_transform.shape) == 3:
        matrix_to_transform = np.diagonal(matrix_to_transform,axis1=1,axis2=2)
        
    out = np.zeros(eigVecs.shape,dtype=complex)
    for ik in range(eigVecs.shape[0]):
        eV = eigVecs[ik,:,:]
        eVT = eV.transpose().conjugate()
        out[ik,:,:] = (np.einsum('ij,jk->ik',(matrix_to_transform[ik,:]*eV),eVT))
    if single:
        out = out[0,:,:]
    return out
    
def fromOrigToDiag(eigVecs,matrix_to_transform):
    

    if len(eigVecs.shape) == 2:
        eigVecs = eigVecs[np.newaxis,:,:]
        matrix_to_transform = matrix_to_transform[np.newaxis,...]
        single = True
    else:
        single = False

    out = np.zeros(eigVecs.shape[:],dtype=complex)
    for ik in range(eigVecs.shape[0]):
        eV = eigVecs[ik,:,:]
        eVT = eV.transpose().conjugate()
        #out[ik,:,:] = (np.einsum('ij,jk,kl->il',eVT,matrix_to_transform[ik,:,:],eV))
        out[ik,:,:] = np.dot(np.dot(eVT,matrix_to_transform[ik,:,:]),eV)
    if single:
        out = out[0,:,:]
    return out
    
def fromDiagToOrig(eigVecs,matrix_to_transform):
    if len(eigVecs.shape) == 2:
        eigVecs = eigVecs[np.newaxis,:,:]
        matrix_to_transform = matrix_to_transform[np.newaxis,...]
        single = True
    else:
        single = False
    eigVecs = np.transpose(eigVecs.conjugate(),axes=(0,2,1))

    out = fromOrigToDiag(eigVecs,matrix_to_transform)
    if single:
        out = out[0,:,:]
    return out
        
