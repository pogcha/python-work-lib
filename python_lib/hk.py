from __future__ import print_function
import numpy as np
import multiprocessing as mp

#import pandas as pd

#def readHkPd(filename,version):
#    broken_df = pd.read_csv('../data/bikes.csv')

def readHk(filename,version):
    '''read a H(k)
    
    Parameters
    ----------
    filename : string
        filename of the text file
    version : int
        defines version of header in H(k)
        1 for nK, nD, nP header,
        2 for 'VERSION 2' style header,
        3 for wannier like header,
        4 for triqs like header
    
    Returns
    ----------
    Hk : ndarray
        hamiltonian of shape (nK,nD,nD)
    nD : int
        total number of orbitals
    nP : int
        number of uncorrelated orbitals
    nAt : int
        number of atoms
    kVec : ndarray
        k vectors of shape (nK,3)
    nE : int
        number of electrons
    '''
    
    print('reading H(k)')
    f = open(filename)
    lines = f.readlines()
    f.close()
    nE = None
    if version == 1:
        nK,nD,nP =  [int(kk) for kk in lines[0].split()[:3]  ]
        
        nT = nD
        nAt = 1
    elif version == 2:
        if ('VERSION 2' in lines[0]) is False:
            raise Exception('error: HK-File is NOT version 2!')
        nK,nAt =  [int(kk) for kk in lines[1].split()  ]
        nD = np.zeros(shape=nAt,dtype=int)
        nP = np.zeros(shape=nAt,dtype=int)
        for iA in range(nAt):
            nD[iA],nP[iA] = [int(kk) for kk in lines[2+iA].split()[:2]  ]
        nT = int(np.sum(nD) + np.sum(nP))
        # hack for srvo3 example from w2dyamics benchmark
        nT = int(np.sum(nP))
    elif version == 3:
        print('   reading LDA++ like header')
        try:
            _,nK,nD,nE =  [int(kk) for kk in lines[0].split()[:4]  ]
        except:
            raise Exception('error: HK-File is possibly NOT version 3! (Wannier-style)')
        nT = nD
        nAt = 1
        nP = 0
    elif version == 4:
        print('   reading triqs header')
        try:
            nK = int(lines[0].split()[0])
        except:
            raise Exception('Error: HK-file is possibly not version 4! (Triqs-style)')
        nE = float(lines[1].split()[0])
        nAt = int(lines[2].split()[0])
        nT = 0
        for iAt in range(nAt):
            nT += int(lines[3+iAt].split()[3])
        nD = int(lines[3+nAt].split()[0])
        #for iC in range(nC):
        #    nD += int(lines[4+nAt+iC].split()[3])
        nP = nT - nD
        
        
    else:
        raise Exception('selected H(k) version number \''+str(version)+'\' not implemented. Choose 1, 2, or 3')
        
            
    Hk = np.zeros(shape=(nK,nT,nT),dtype=complex)
    print('info from header:')
    print('nk =',nK)
    print('nOrbs =',nT)
    print('nD =',nD)
    kVec = np.zeros(shape=(nK,3),dtype=float)
    if version == 1 or version == 3:
        lineCount = 1
    elif version == 2:
        lineCount = 3+nAt
    elif version == 4:
        lineCount = 4+nAt+2*nD

    for iK in range(nK):
        if version == 4:
            for iB in range(nT):
                splitted = lines[lineCount].split()
                dummy = [float(kk) for kk in splitted  ]
                Hk[iK,iB,:] += dummy
                lineCount += 1
            for iB in range(nT):
                splitted = lines[lineCount].split()
                dummy = [float(kk) for kk in splitted  ]
                Hk[iK,iB,:] += 1j*np.array(dummy,dtype=complex)
                lineCount += 1
        else:
            kVec[iK,:]=  [float(kk) for kk in lines[lineCount].split()  ]
            lineCount += 1
            for iB in range(nT):
                splitted = lines[lineCount].split()
                dummy = [float(kk) for kk in splitted  ]
                lineCount += 1
                Hk[iK,iB,:] = dummy[0::2] + 1j*np.array(dummy[1::2],dtype=complex)
    return Hk, nD, nP, nAt, kVec, nE

def writeHk(filename,version,hk,kVec,nAt,nD,nP,nE):
    ''' writes a hamiltonian H(k) in w2wannier format. Dimensions are Hk[nk, nOrb, nOrb]
    
    Parameters
    ----------
    version : int
        toggles the version of the header,
        1 for nk, nD, nP (number of k-points, number of d-orbitals, number of p-orbitals)
        2 for 'VERSION 2' like header 
        3 for wannier like header
    nS : int
        number of spins
    nK : int
        number of k-points
    nO : int
        number of orbitals
    nE : float
        number of electrons
    '''
    print('writing H(k)')
    
    numK = hk.shape[0]
    numOrb = hk.shape[1]

    if version == 1:
        header = ' {0:d}  {1:d}  {2:d}  # k-points, orbs, orbs\n'.format(numK,numOrb,numOrb)
    elif version == 2:
        header = 'VERSION 2\n'
        header = header + '{0:d} {1:d}\n'.format(numK,nAt)
        for iA in range(nAt):
            header = header + '{0:d} {1:d}\n'.format(nD[iA],nP[iA])
    elif version == 3:
        header = ' {0:d}  {1:d}  {2:d}  {3:d} # of spins, # of k-points, # of orbitals, # of electrons \n'.format(1,numK,numOrb,nE) 
    else:
        raise Exception('selected H(k) version number \''+str(version)+'\' not implemented. Choose 1, 2, or 3')
    
    with open(filename,'w') as f:        
        f.write(header)
        for iK in range(numK):
            f.write('  {0:1.9f}   {1:1.9f}   {2:1.9f}\n'.format(kVec[iK,0],kVec[iK,1],kVec[iK,2]))
            for iB in range(numOrb):
                for jB in range(numOrb):
                    f.write('  {0:1.14f}  {1:1.14f}'.format(hk[iK,iB,jB].real,hk[iK,iB,jB].imag))
                f.write('\n')
    
def fourier(fk,kVec,rVec):
    # assume kVec is a [Nk x dim] array
    # assume rVec is a [Nr x dim] array
    # assume dimension of k and r are the same
    # assume Nk in kVec and Nk in Hk are the same
    
    assert len(kVec.shape) == 2
    assert len(rVec.shape) == 2
        
    assert kVec.shape[0] == fk.shape[0]
    
    dimK = kVec.shape[1]
    dimR = rVec.shape[1]
    assert dimK == dimR

    numK = kVec.shape[0]
    
    # calculate exponential
    exp = np.exp(-1j*np.sum(kVec[np.newaxis,:,:]*rVec[:,np.newaxis,:],axis=2))
    # add as many axis as needed to get the same dimension as fk
    for ii in range(len(fk.shape)-1):
        exp = exp[:,:,np.newaxis]
    # do the sum over r
    fr = 1.0/float(numK) * np.sum(exp*fk[np.newaxis,...],axis=1)

    return fr
    


def k_line(point_list,nK):
    
    kVec_x = []
    kVec_y = []
    kVec_z = []
    nK_all = len(point_list)*nK
    for part in point_list:
        kVec_x.append(np.linspace(part[0][0],part[1][0],nK,endpoint=False))
        kVec_y.append(np.linspace(part[0][1],part[1][1],nK,endpoint=False))
        kVec_z.append(np.linspace(part[0][2],part[1][2],nK,endpoint=False))
    kVec_x = np.reshape(np.array(kVec_x),nK_all)
    kVec_y = np.reshape(np.array(kVec_y),nK_all)
    kVec_z = np.reshape(np.array(kVec_z),nK_all)
    
    kVec = np.array([kVec_x,kVec_y,kVec_z]).transpose()

    return kVec
    
def process_k_chunk(kVec_chunk,fr,rVec,deg_r):
    nk_chunk = kVec_chunk.shape[0]
    fk_chunk = np.zeros((nk_chunk,fr.shape[1],fr.shape[2]),dtype=complex)
    for ik in range(nk_chunk):
        exp = np.exp(-1j*2*np.pi*np.sum(kVec_chunk[ik,:]*rVec[:,:],axis=1))/deg_r
        fk_chunk[ik,:,:] = np.sum(exp[:,np.newaxis,np.newaxis]*fr,axis=0)
    return fk_chunk
        
def fourier_wannier_r_to_k(fr,kVec,rVec,deg_r,low_mem=False):
    # assume kVec is a [Nk x dim] array
    # assume rVec is a [Nr x dim] array
    # assume dimension of k and r are the same
    # assume Nk in kVec and Nk in Hk are the same
    
    assert len(kVec.shape) == 2
    assert len(rVec.shape) == 2
        
    assert rVec.shape[0] == fr.shape[0]
    
    dimK = kVec.shape[1]
    dimR = rVec.shape[1]
    assert dimK == dimR

    print('calculating fourier transform from r to k space...')
    
    # calculate exponential
    if low_mem == False:
        try:
            exp = np.exp(-1j*2*np.pi*np.sum(kVec[np.newaxis,:,:]*rVec[:,np.newaxis,:],axis=2))/deg_r[:,np.newaxis]
            fk = np.sum(exp[:,:,np.newaxis,np.newaxis]*fr[:,np.newaxis,:,:],axis=0)
        except MemoryError as e:
            print(e)
            print('not enough memory to perform calculation of exp(ikr) with numpy broadcasting. doing k-loop')
            low_mem=True
            
    if low_mem == True:
        nk = kVec.shape[0]
        fk = np.zeros((nk,fr.shape[1],fr.shape[2]),dtype=complex)
        if False:
            n_proc = mp.cpu_count()
            n_proc = 1
            proc_chunks = []
            
            k_ind = list(range(nk))
            chunksize = nk // n_proc
            for i_proc in range(n_proc):
                chunkstart = i_proc * chunksize
                # make sure to include the division remainder for the last process
                chunkend = (i_proc + 1) * chunksize if i_proc < n_proc - 1 else None
            
                proc_chunks.append(kVec[chunkstart: chunkend,:])
            assert sum(map(len, proc_chunks)) == nk
            
            #print(proc_chunks)
            #print(n_proc)
            #print()
            
            with mp.Pool(processes=n_proc) as pool:
                # starts the sub-processes without blocking
                # pass the chunk to each worker process
                proc_results = [pool.apply_async(process_k_chunk,
                                                 args=(chunk,fr,rVec,deg_r))
                                for chunk in proc_chunks]
            
                # blocks until all results are fetched
                #print(proc_results)
                result_chunks = [r.get() for r in proc_results]
            #print(result_chunks)
            k_list = list(range(nk))
            
            
            for i_proc in range(n_proc):
                chunkstart = i_proc * chunksize
                # make sure to include the division remainder for the last process
                chunkend = (i_proc + 1) * chunksize if i_proc < n_proc - 1 else None
                fk[chunkstart:chunkend,...] = result_chunks[i_proc]
        else:
            for ik in range(nk):
                exp = np.exp(-1j*2*np.pi*np.sum(kVec[ik,:]*rVec[:,:],axis=1))/deg_r
                fk[ik,:,:] = np.sum(exp[:,np.newaxis,np.newaxis]*fr,axis=0)
            
    print('done fourier transforming!')
    return fk

def read_hr_wannier(seedname):
    
    with open(seedname+'_hr.dat') as f:
        hr_data = f.readlines()
    
    nWF = int(hr_data[1])
    nR = int(hr_data[2])
    
    print('nR',nR)
    print('nWF',nWF)
    
    rVec = np.zeros((nR,3),dtype=int)
    deg_r = np.zeros(nR,dtype=int)
    hr = np.zeros((nR,nWF,nWF),dtype=complex)
    
    line = 2
    
    iR = 0
    while iR < nR:
        line += 1
        for x in hr_data[line].split():
            deg_r[iR] = int(x)
            iR += 1
            
    for iR in range(nR):
        for ii in range(nWF):
            for jj in range(nWF):
                line += 1
                line_data = hr_data[line].split()
                if ii == 0 and jj == 0:
                    rVec[iR,:] = [int(x) for x in line_data[:3]]
                hr[iR,ii,jj] = complex(float(line_data[5]), float(line_data[6]))
    
    
    
    return nWF,nR, rVec,deg_r, hr
    
