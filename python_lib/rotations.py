#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  4 09:25:36 2019

@author: mschueler
"""

import numpy as np

# euler rotation

def twodim_rot(alpha):
    return np.array([[np.cos(alpha), -np.sin(alpha)],
              [np.sin(alpha),  np.cos(alpha)]])
    
    
def euler_rot(alpha,beta,gamma):
    
    rot1 = np.eye(3)
    rot2 = np.eye(3)
    rot3 = np.eye(3)
    
    rot1[:2,:2] = twodim_rot(alpha)
    rot2[1:,1:] = twodim_rot(beta)
    rot3[:2,:2] = twodim_rot(gamma)
    
    return np.dot(rot1,np.dot(rot2,rot3))
