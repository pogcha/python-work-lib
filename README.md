# Modules

This collection contains python modules to

* load a H(k) file in a wannier90 like format
   * `hk.py`
* generate 4-index Coulomb tensors
  * `coulomb.py`
* calculate interacting and noninteracting greens function for H(k)'s, calculate hybridization functions and density matrices for H(k)'s
  * `greens_functions.py`
* apply lorentian broadening to data, i.e., spectra or DOS. Do a Savitzky-Golay smoothing.
  * `smearBib.py`
* read partial DOS and DOS from VASP output into python
  * `vasp_parse.py`
* do some linear algebra related to greens functions: Transform matrices from eigenbasis to original basis and vice versa...
  * `linalg.py`
* fit, evaluate, and calculate the gradient of 2d polynomials
 * `poly_2d.py`
* insert a progress bar into a (long lasting) loop
  * `misc.py`

# Examples
There are some examples to
* calculate a DOS from a H(k) via calculating the greensfuntion for NbSe2 and compares to VASP
  * `examples/DOS_from_HK_nbse2/checkHk.py`
* calculate a Coulomb tensor for s, p, d, and f shells and makes an Umatrix.dat output which can be read by Indras ED-code
  * `examples/coulomb/u_matrix_for_ED_Indra.py`
* Fit noisy 2d test data by a 2d polynomial. Compare the gradient with the result from the fit. Also compare the Sav.-Golay approach.
  * `examples/poly_2d_example.py`

# Scripts
There are standalone scripts for using in bash to
* Smear partial and total DOS from vasp
  * `scripts/smearVASP.py`
* Calculate DOS (total and partial) from a H(k) file: Usage `dosHk.py filename [options]`
  * `scripts/dosHk.py`
* Read a H(k) file and write it back with less k-points. Good for generating small H(k)'s for debugging
  * `scripts/skipHk.py`
* Read hdf5 file from w2dynamics and estimate optimal tauDiffMax
  * `scripts/set_tau_diff_max.py`
* Read hdf5 file from w2dynamics and estimate optimal NCorr
  * `scripts/set_NCorr.py`
* Convert some quantity with units from SI to natural units or vice versa
  * `scripts/convert_SI_Natural.py`


# Help section

## scripts/convert_SI_Natural.py
The SI unit system and the natural unit system are equivalent ways of describing physical quantities. SI has the basis (meter, second, kilogram and Ampere). The natural system has the basis (speed of light, \hbar, electron volt, and 4 \pi \varepsilon_0).
This python script makes it easy and transparent to convert quantities between these systems. It is a simple command line tool, which is invoked with the value you want to convert, its unit system and the powers of its units. An easy example would be to check, if the speed of light, 2.998 108 m/s, is ‘one’ in natural units. The power of meter is 1, the power of seconds is -1. The powers of the remaining kg and A are both 0. Running the code then looks like this
```
convert_SI_Natural.py --unit=si -- 2.998e8 1 -1 0 0
 converted
    2.998000000e+08 m s^-1
     = 1.000025157e+00 c
```
Looks good! Now lets check the opposite direction. The power of c is 1. The powers of the remaining units (\hbar, eV and 4\pi \varepsilon_0) are zero.
```
convert_SI_Natural.py --unit=nat -- 1.0 1 0 0 0
 converted
    1.000000000e+00 c
     = 2.997924580e+08 m s^-1
```
This is the value by NIST definition. All the details for using the script can be found by invoking the script with
```
convert_SI_Natural.py --help
```