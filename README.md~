This collection contains python scripts to

* load a H(k) file in a wannier90 like format
   * `hk.py`
* generate 4-index Coulomb tensors
  * `coulomb.py`
* calculate interacting and noninteracting greens function for H(k)'s and calculate hybridization functions
  * `greens_functions.py`
* apply lorentian broadening to data, i.e., spectra or DOS
  * `smearBib.py`
* read partial DOS and DOS from VASP output into python
  * `vasp_parse.py`


There are some examples to
* calculate a DOS from a H(k) via calculating the greensfuntion for NbSe2 and compares to VASP
  * `examples/DOS_from_HK_nbse2/checkHk.py`
* calculate a Coulomb tensor for s, p, d, and f shells and makes an Umatrix.dat output which can be read by Indras ED-code
  * `examples/coulomb/u_matrix_for_ED_Indra.py`

There are standalone scripts for using in bash to
* Smear partial and total DOS from vasp
  * `scripts/smearVASP.py`
* Calculate DOS (total and partial) from a H(k) file
  * `scripts/dosHk.py`
